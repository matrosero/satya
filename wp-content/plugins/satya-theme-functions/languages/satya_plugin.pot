#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Satya Theme Functions\n"
"POT-Creation-Date: 2020-08-06 15:48-0600\n"
"PO-Revision-Date: 2019-01-26 21:54-0600\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: satya-theme-functions.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;"
"_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: includes/acf/blocks.php:12 includes/cpt/register-cpt.php:96
#: includes/cpt/register-cpt.php:108
msgid "Therapies"
msgstr ""

#: includes/acf/blocks.php:13
msgid "Show therapies block."
msgstr ""

#: includes/acf/blocks.php:23
msgid "Satya featured text"
msgstr ""

#: includes/acf/blocks.php:24
msgid "Full width larger text with shape"
msgstr ""

#: includes/acf/options-pages.php:5
msgid "Satya Contact Settings"
msgstr ""

#: includes/acf/options-pages.php:6
msgid "Satya Contacto"
msgstr ""

#: includes/acf/options-pages.php:15
msgid "Schedule Settings"
msgstr ""

#: includes/acf/options-pages.php:16
msgid "Schedule"
msgstr ""

#: includes/admin-tweaks/excerpt.php:9
msgid "Excerpt"
msgstr ""

#: includes/cpt/register-cpt.php:20 includes/cpt/register-cpt.php:32
msgid "Classes"
msgstr ""

#: includes/cpt/register-cpt.php:21
msgid "Class"
msgstr ""

#: includes/cpt/register-cpt.php:22
msgid "Add New Class"
msgstr ""

#: includes/cpt/register-cpt.php:23
msgid "Edit Class"
msgstr ""

#: includes/cpt/register-cpt.php:24
msgid "New Class"
msgstr ""

#: includes/cpt/register-cpt.php:25
msgid "View Class"
msgstr ""

#: includes/cpt/register-cpt.php:26
msgid "View Classes"
msgstr ""

#: includes/cpt/register-cpt.php:27
msgid "Search Classes"
msgstr ""

#: includes/cpt/register-cpt.php:28
msgid "No Classes found"
msgstr ""

#: includes/cpt/register-cpt.php:29
msgid "No Classes found in Trash"
msgstr ""

#: includes/cpt/register-cpt.php:58 includes/cpt/register-cpt.php:70
msgid "Events"
msgstr ""

#: includes/cpt/register-cpt.php:59
msgid "Event"
msgstr ""

#: includes/cpt/register-cpt.php:60
msgid "Add New Event"
msgstr ""

#: includes/cpt/register-cpt.php:61
msgid "Edit Event"
msgstr ""

#: includes/cpt/register-cpt.php:62
msgid "New Event"
msgstr ""

#: includes/cpt/register-cpt.php:63
msgid "View Event"
msgstr ""

#: includes/cpt/register-cpt.php:64
msgid "View Events"
msgstr ""

#: includes/cpt/register-cpt.php:65
msgid "Search Events"
msgstr ""

#: includes/cpt/register-cpt.php:66
msgid "No Events found"
msgstr ""

#: includes/cpt/register-cpt.php:67
msgid "No Events found in Trash"
msgstr ""

#: includes/cpt/register-cpt.php:97
msgid "Therapy"
msgstr ""

#: includes/cpt/register-cpt.php:98
msgid "Add New Therapy"
msgstr ""

#: includes/cpt/register-cpt.php:99
msgid "Edit Therapy"
msgstr ""

#: includes/cpt/register-cpt.php:100
msgid "New Therapy"
msgstr ""

#: includes/cpt/register-cpt.php:101
msgid "View Therapy"
msgstr ""

#: includes/cpt/register-cpt.php:102
msgid "View Therapies"
msgstr ""

#: includes/cpt/register-cpt.php:103
msgid "Search Therapies"
msgstr ""

#: includes/cpt/register-cpt.php:104
msgid "No Therapies found"
msgstr ""

#: includes/cpt/register-cpt.php:105
msgid "No Therapies found in Trash"
msgstr ""

#: includes/cpt/register-cpt.php:134 includes/cpt/register-cpt.php:135
msgid "Person"
msgstr ""

#: includes/cpt/register-cpt.php:136
msgid "Add New Person"
msgstr ""

#: includes/cpt/register-cpt.php:137
msgid "Edit Person"
msgstr ""

#: includes/cpt/register-cpt.php:138
msgid "New Person"
msgstr ""

#: includes/cpt/register-cpt.php:139
msgid "View Person"
msgstr ""

#: includes/cpt/register-cpt.php:140
msgid "View People"
msgstr ""

#: includes/cpt/register-cpt.php:141
msgid "Search People"
msgstr ""

#: includes/cpt/register-cpt.php:142
msgid "No People found"
msgstr ""

#: includes/cpt/register-cpt.php:143
msgid "No People found in Trash"
msgstr ""

#: includes/cpt/register-cpt.php:146
msgid "People"
msgstr ""

#: includes/cpt/register-taxonomies.php:16
msgctxt "Taxonomy General Name"
msgid "Roles"
msgstr ""

#: includes/cpt/register-taxonomies.php:17
msgctxt "Taxonomy Singular Name"
msgid "Role"
msgstr ""

#: includes/cpt/register-taxonomies.php:18
msgid "Role"
msgstr ""

#: includes/cpt/register-taxonomies.php:19
msgid "All Roles"
msgstr ""

#: includes/cpt/register-taxonomies.php:20
msgid "Parent Role"
msgstr ""

#: includes/cpt/register-taxonomies.php:21
msgid "Parent Role:"
msgstr ""

#: includes/cpt/register-taxonomies.php:22
msgid "New Role Name"
msgstr ""

#: includes/cpt/register-taxonomies.php:23
msgid "Add New Role"
msgstr ""

#: includes/cpt/register-taxonomies.php:24
msgid "Edit Role"
msgstr ""

#: includes/cpt/register-taxonomies.php:25
msgid "Update Role"
msgstr ""

#: includes/cpt/register-taxonomies.php:26
msgid "View Role"
msgstr ""

#: includes/cpt/register-taxonomies.php:27
#: includes/cpt/register-taxonomies.php:68
#: includes/cpt/register-taxonomies.php:110
#: includes/cpt/register-taxonomies.php:147
msgid "Separate items with commas"
msgstr ""

#: includes/cpt/register-taxonomies.php:28
#: includes/cpt/register-taxonomies.php:69
#: includes/cpt/register-taxonomies.php:111
#: includes/cpt/register-taxonomies.php:148
msgid "Add or remove items"
msgstr ""

#: includes/cpt/register-taxonomies.php:29
#: includes/cpt/register-taxonomies.php:70
#: includes/cpt/register-taxonomies.php:112
#: includes/cpt/register-taxonomies.php:149
msgid "Choose from the most used"
msgstr ""

#: includes/cpt/register-taxonomies.php:30
msgid "Popular Roles"
msgstr ""

#: includes/cpt/register-taxonomies.php:31
msgid "Search Roles"
msgstr ""

#: includes/cpt/register-taxonomies.php:32
#: includes/cpt/register-taxonomies.php:73
#: includes/cpt/register-taxonomies.php:115
#: includes/cpt/register-taxonomies.php:152
msgid "Not Found"
msgstr ""

#: includes/cpt/register-taxonomies.php:33
#: includes/cpt/register-taxonomies.php:74
#: includes/cpt/register-taxonomies.php:116
#: includes/cpt/register-taxonomies.php:153
msgid "No items"
msgstr ""

#: includes/cpt/register-taxonomies.php:34
msgid "Roles list"
msgstr ""

#: includes/cpt/register-taxonomies.php:35
msgid "Roles list navigation"
msgstr ""

#: includes/cpt/register-taxonomies.php:57
msgctxt "Taxonomy General Name"
msgid "Therapists"
msgstr ""

#: includes/cpt/register-taxonomies.php:58
msgctxt "Taxonomy Singular Name"
msgid "Therapist"
msgstr ""

#: includes/cpt/register-taxonomies.php:59
msgid "Therapist"
msgstr ""

#: includes/cpt/register-taxonomies.php:60
msgid "All Therapists"
msgstr ""

#: includes/cpt/register-taxonomies.php:61
msgid "Parent Therapist"
msgstr ""

#: includes/cpt/register-taxonomies.php:62
msgid "Parent Therapist:"
msgstr ""

#: includes/cpt/register-taxonomies.php:63
msgid "New Therapist Name"
msgstr ""

#: includes/cpt/register-taxonomies.php:64
msgid "Add New Therapist"
msgstr ""

#: includes/cpt/register-taxonomies.php:65
msgid "Edit Therapist"
msgstr ""

#: includes/cpt/register-taxonomies.php:66
msgid "Update Therapist"
msgstr ""

#: includes/cpt/register-taxonomies.php:67
msgid "View Therapist"
msgstr ""

#: includes/cpt/register-taxonomies.php:71
msgid "Popular Therapists"
msgstr ""

#: includes/cpt/register-taxonomies.php:72
msgid "Search Therapists"
msgstr ""

#: includes/cpt/register-taxonomies.php:75
msgid "Therapists list"
msgstr ""

#: includes/cpt/register-taxonomies.php:76
msgid "Therapists list navigation"
msgstr ""

#: includes/cpt/register-taxonomies.php:99
msgctxt "Taxonomy General Name"
msgid "Types"
msgstr ""

#: includes/cpt/register-taxonomies.php:100
msgctxt "Taxonomy Singular Name"
msgid "Type"
msgstr ""

#: includes/cpt/register-taxonomies.php:101
msgid "Type"
msgstr ""

#: includes/cpt/register-taxonomies.php:102
msgid "All Types"
msgstr ""

#: includes/cpt/register-taxonomies.php:103
msgid "Parent Type"
msgstr ""

#: includes/cpt/register-taxonomies.php:104
msgid "Parent Type:"
msgstr ""

#: includes/cpt/register-taxonomies.php:105
msgid "New Type Name"
msgstr ""

#: includes/cpt/register-taxonomies.php:106
msgid "Add New Type"
msgstr ""

#: includes/cpt/register-taxonomies.php:107
msgid "Edit Type"
msgstr ""

#: includes/cpt/register-taxonomies.php:108
msgid "Update Type"
msgstr ""

#: includes/cpt/register-taxonomies.php:109
msgid "View Type"
msgstr ""

#: includes/cpt/register-taxonomies.php:113
msgid "Popular Types"
msgstr ""

#: includes/cpt/register-taxonomies.php:114
msgid "Search Types"
msgstr ""

#: includes/cpt/register-taxonomies.php:117
msgid "Types list"
msgstr ""

#: includes/cpt/register-taxonomies.php:118
msgid "Types list navigation"
msgstr ""

#: includes/cpt/register-taxonomies.php:136
msgctxt "Taxonomy General Name"
msgid "Permanences"
msgstr ""

#: includes/cpt/register-taxonomies.php:137
msgctxt "Taxonomy Singular Name"
msgid "Permanence"
msgstr ""

#: includes/cpt/register-taxonomies.php:138
msgid "Permanence"
msgstr ""

#: includes/cpt/register-taxonomies.php:139
msgid "All Permanences"
msgstr ""

#: includes/cpt/register-taxonomies.php:140
msgid "Parent Permanence"
msgstr ""

#: includes/cpt/register-taxonomies.php:141
msgid "Parent Permanence:"
msgstr ""

#: includes/cpt/register-taxonomies.php:142
msgid "New Permanence Name"
msgstr ""

#: includes/cpt/register-taxonomies.php:143
msgid "Add New Permanence"
msgstr ""

#: includes/cpt/register-taxonomies.php:144
msgid "Edit Permanence"
msgstr ""

#: includes/cpt/register-taxonomies.php:145
msgid "Update Permanence"
msgstr ""

#: includes/cpt/register-taxonomies.php:146
msgid "View Permanence"
msgstr ""

#: includes/cpt/register-taxonomies.php:150
msgid "Popular Permanences"
msgstr ""

#: includes/cpt/register-taxonomies.php:151
msgid "Search Permanences"
msgstr ""

#: includes/cpt/register-taxonomies.php:154
msgid "Permanences list"
msgstr ""

#: includes/cpt/register-taxonomies.php:155
msgid "Permanences list navigation"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "Satya Theme Functions"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "https://gitlab.com/matrosero/satya"
msgstr ""

#. Description of the plugin/theme
msgid "Custom functions for Satya's website."
msgstr ""

#. Author of the plugin/theme
msgid "Mat Rosero"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://www.matilderosero.com/"
msgstr ""
