��    v      �  �   |      �	     �	     �	     
      
     /
     <
     N
     ^
     k
     
  	   �
     �
  	   �
     �
     �
     �
  %   �
  
      
             &  	   2     <     K  	   X     b     h     o  !   w  
   �  	   �  	   �     �  
   �     �     �     �               #     =     M     f     v     �     �     �  	   �     �     �     �                     2     >     K  
   R     ]     n     �     �     �     �     �     �  
   �     �     �          '     ;     D     V     e     s     �     �     �     �     �     �     �  !        $      @     a  !   }     �      �     �  	   �  	                  7     ?  
   D     O     e     w     �     �  
   �     �  
   �     �     �     �     �  	   �                 	   ,  "   6     Y  �  x     )     =     R     l     �     �     �     �     �     �       
        "     2     N     T  4   [     �     �     �     �  
   �     �     �     �                 )     
   F     Q     ]     j     �     �     �     �     �     �  !        $  )   A     k  #   �     �  )   �     �               !  	   4  
   >     I     Y  
   j     u     �     �     �     �     �     �     �     �                    +      =     ^     w     �     �     �     �     �     �     �     �          !     .     M     h     u  
   {     �     �     �  	   �     �     �  	   �     �     �     �     �     �               +     :     P  	   `  
   j  
   u     �     �     �     �     �     �     �     �     �  "   �                 ^       m   >   N       C       @      L       u   X   n   I   5   p      r   M   <      :          %   .          )   ]   B   U   d       9   &       j       F   _   [       #              W       +       $   g          4      f             7   V   '           O   8   (       S   k          !      0   ,   s   t      Z       K   P   ?                       	   -           A   v   3   R              o   J                      E   q      2   /      c   *   
   l   i   1   6   `                 b   \                  ;   T   H          D   =   e   Y   Q   "      G      a           h    Add New Class Add New Event Add New Permanence Add New Person Add New Role Add New Therapist Add New Therapy Add New Type Add or remove items All Permanences All Roles All Therapists All Types Choose from the most used Class Classes Custom functions for Satya's website. Edit Class Edit Event Edit Permanence Edit Person Edit Role Edit Therapist Edit Therapy Edit Type Event Events Excerpt Full width larger text with shape Mat Rosero New Class New Event New Permanence Name New Person New Role Name New Therapist Name New Therapy New Type Name No Classes found No Classes found in Trash No Events found No Events found in Trash No People found No People found in Trash No Therapies found No Therapies found in Trash No items Not Found Parent Permanence Parent Permanence: Parent Role Parent Role: Parent Therapist Parent Therapist: Parent Type Parent Type: People Permanence Permanences list Permanences list navigation Person Popular Permanences Popular Roles Popular Therapists Popular Types Role Roles list Roles list navigation Satya Contact Settings Satya Theme Functions Satya featured text Schedule Schedule Settings Search Classes Search Events Search People Search Permanences Search Roles Search Therapies Search Therapists Search Types Separate items with commas Show therapies block. Taxonomy General NamePermanences Taxonomy General NameRoles Taxonomy General NameTherapists Taxonomy General NameTypes Taxonomy Singular NamePermanence Taxonomy Singular NameRole Taxonomy Singular NameTherapist Taxonomy Singular NameType Therapies Therapist Therapists list Therapists list navigation Therapy Type Types list Types list navigation Update Permanence Update Role Update Therapist Update Type View Class View Classes View Event View Events View People View Permanence View Person View Role View Therapies View Therapist View Therapy View Type https://gitlab.com/matrosero/satya https://www.matilderosero.com/ Project-Id-Version: Satya Theme Functions
PO-Revision-Date: 2020-08-06 15:49-0600
Last-Translator: 
Language-Team: 
Language: es_PE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: satya-theme-functions.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Agregar nueva clase Agregar nuevo evento Añadir nueva permanencia Agregar nueva persona Agregar nuevo rol Agregar nuevo terapeuta Agregar nueva terapia Agregar nuevo tipo Agregar o quitar elementos Todas las permanencias Todos los roles Terapeutas Todos los tipos Elija entre los más usados Clase Clases Funciones personalizadas para el sitio web de Satya. Editar clase Editar evento Editar permanencia Editar persona Editar rol Editar terapeuta Editar terapia Editar Tipo Evento Eventos Extracto Texto grande de ancho completo con imagen Mat Rosero Nueva clase Nuevo evento Nuevo nombre de permanencia Nueva persona Nuevo nombre de rol Nuevo nombre del terapeuta Nueva terapia Nuevo nombre de tipo No se han encontrado clases No se encontraron clases en Trash No se han encontrado eventos No hay eventos encontrados en la papelera No se encontraron personas No se encontraron personas en Trash No se encontraron terapias No se encontraron terapias en la papelera No hay artículos No Encontrado Permanencia padre Permanencia padre: Rol padre Rol padre: Terapeuta padre Terapeuta padre: Tipo padre Tipo padre: Personas Permanencia Lista de permanencias Navegación permanencias Persona Permanences populares Roles populares Terapeutas populares Tipos populares Rol Lista de roles Navegación roles Configuración de contacto Satya Funciones del tema Satya Satya texto importante Horario Configuración del horario Buscar clases Buscar eventos Buscar personas Buscar permanencias Buscar roles Buscar terapias Buscar terapeutas Buscar tipos Separe los elementos con comas No se encontraron terapias Permanencias Roles Terapeutas Tipos Permanencia Rol Terapeuta Tipo Terapias Terapeuta Lista de terapeutas Navegación terapeutas Terapia Tipo Lista tipos Navegación tipos Actualizar permanencia Actualizar rol Actualizar terapeutas Actualizar tipo Ver clase Ver clases Ver evento Ver eventos Ver personas Ver permanencia Ver persona Ver rol Ver terapias Ver terapeuta Ver terapia Ver tipo https://gitlab.com/matrosero/Satya https://www.matilderosero.com/ 