<?php
/*
Plugin Name: Satya Theme Functions
Plugin URI: https://gitlab.com/matrosero/satya
Description: Custom functions for Satya's website.
Version: 0.1
Author: Mat Rosero
Author URI: https://www.matilderosero.com/
This plugin is released under the GPLv2 license. The images packaged with this plugin are the property of their respective owners, and do not, necessarily, inherit the GPLv2 license.
*/


/**
 * Load plugin textdomain.
 *
 * @since 0.1.0
 */
function satya_load_textdomain() {
	load_plugin_textdomain( 'satya_plugin', false, basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action( 'plugins_loaded', 'satya_load_textdomain' );


/**
 * Get the CMB2 bootstrap!
 *
 * @since 0.1.0
 */
// if ( file_exists( __DIR__ . '/vendor/cmb2/init.php' ) ) {
//   	require_once __DIR__ . '/vendor/cmb2/init.php';
// } elseif ( file_exists(  __DIR__ . '/vendor/CMB2/init.php' ) ) {
//   	require_once __DIR__ . '/vendor/CMB2/init.php';
// }





/**
 * Post types.
 *
 * @since 0.1.0
 */
require_once( dirname( __FILE__ ) . '/includes/cpt/register-cpt.php' );
require_once( dirname( __FILE__ ) . '/includes/cpt/register-taxonomies.php' );
// require_once( dirname( __FILE__ ) . '/includes/cpt/cpt-tax-helpers.php' );

/**
 * Custom fields.
 *
 * @since 0.1.0
 */
require_once( dirname( __FILE__ ) . '/includes/acf/options-pages.php' );
require_once( dirname( __FILE__ ) . '/includes/acf/hooks.php' );
require_once( dirname( __FILE__ ) . '/includes/acf/blocks.php' );
// require_once( dirname( __FILE__ ) . '/includes/cmb2/register-event-metabox-fields.php' );
// require_once( dirname( __FILE__ ) . '/includes/cmb2/register-taxonomy-metabox-fields.php' );
// require_once( dirname( __FILE__ ) . '/includes/cmb2/filters.php' );
// require_once( dirname( __FILE__ ) . '/includes/cmb2/cmb2-helpers.php' );
// require_once( dirname( __FILE__ ) . '/includes/cmb2/cmb2-admin-styles.php' );


/**
 * Enqueue scripts.
 *
 * @since 0.1.0
 */
// require_once( dirname( __FILE__ ) . '/includes/enqueue.php' );


/**
 * Admin tweaks.
 *
 * @since 0.1.0
 */
require_once( dirname( __FILE__ ) . '/includes/admin-tweaks/columns.php' );
require_once( dirname( __FILE__ ) . '/includes/admin-tweaks/excerpt.php' );