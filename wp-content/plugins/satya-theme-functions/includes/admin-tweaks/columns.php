<?php

function add_featured_image_column($defaults) {
    $defaults['featured_image'] = 'Featured Image';
    return $defaults;
}
add_filter('manage_posts_columns', 'add_featured_image_column');
add_filter('manage_pages_columns', 'add_featured_image_column');
// add_filter('manage_therapy_posts_columns', 'add_featured_image_column');
// add_filter('manage_people_posts_columns', 'add_featured_image_column');
// add_filter('manage_class_posts_columns', 'add_featured_image_column');
// add_filter('manage_event_posts_columns', 'add_featured_image_column');
 
function show_featured_image_column($column_name, $post_id) {
    if ($column_name == 'featured_image') {
        echo get_the_post_thumbnail($post_id, 'thumbnail'); 
    }
}
add_action('manage_posts_custom_column', 'show_featured_image_column', 10, 2);
add_action('manage_pages_custom_column', 'show_featured_image_column', 10, 2);
// add_action('manage_therapy_posts_custom_column', 'show_featured_image_column', 10, 2);
// add_action('manage_people_posts_custom_column', 'show_featured_image_column', 10, 2);
// add_action('manage_class_posts_custom_column', 'show_featured_image_column', 10, 2);
// add_action('manage_event_posts_custom_column', 'show_featured_image_column', 10, 2);


// add_filter('manage_posts_columns', 'satya_posts_columns', 5);
// add_action('manage_posts_custom_column', 'satya_posts_custom_columns', 5, 2);
 
// function satya_posts_columns($defaults){
//     $defaults['featured_image'] = __('Featured image');
//     return $defaults;
// }
 
// function satya_posts_custom_columns($column_name, $id){
//     if($column_name === 'featured_image'){
//         echo the_post_thumbnail( 'thumbnail' );
//     }
// }