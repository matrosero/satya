jQuery(function($){
  $(document).ready(function() {

    var trigger = $("#toggle-menu, #close-panel"),
        panel = $('.offcanvas-panel'),
        panelContent = $('.panel-container');


    //open/close primary navigation
    trigger.on('click', function(e){
      e.preventDefault();
      
      //in firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
      if( panel.hasClass('is-open') ) {
        
        panel.removeClass('is-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',function(){
          $('body').removeClass('overflow-hidden');
          panel.removeClass('is-open is-visible');
        });

      } else {
        
        panel.addClass('is-open is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',function(){
          $('body').addClass('overflow-hidden');
        }); 

      }
    });

    // if (Modernizr.cssgrid,cssgridlegacy) {}
  });
});