<?php

/**
 * Register post types.
 *
 * @since 0.1.0
 */


if ( ! function_exists('satya_register_cpt') ) {

	// Register Custom Post Type
	function satya_register_cpt () {


		//Class CTP
		register_post_type( 'class',
			array(
				$labels = array(
					'name' => __( 'Classes', 'satya_plugin' ),
					'singular_name' => __( 'Class', 'satya_plugin' ),
					'add_new_item' => __( 'Add New Class', 'satya_plugin' ),
					'edit_item' => __( 'Edit Class', 'satya_plugin' ),
					'new_item' => __( 'New Class', 'satya_plugin' ),
					'view_item' => __( 'View Class', 'satya_plugin' ),
					'view_items' => __( 'View Classes', 'satya_plugin' ),
					'search_items' => __( 'Search Classes', 'satya_plugin' ),
					'not_found' => __( 'No Classes found', 'satya_plugin' ),
					'not_found_in_trash' => __( 'No Classes found in Trash', 'satya_plugin' )
				),

				'label'                 => __( 'Classes', 'satya_plugin' ),
				'labels'                => $labels,
				'public'                => true,
				'exclude_from_search'   => false, //*
				'publicly_queryable'    => true, //*
				'show_ui'               => true,
				'show_in_nav_menus'     => false, //*
				'show_in_menu'          => true,
				'show_in_admin_bar'     => true,
				'menu_position'         => 20,
				'menu_icon'             => 'dashicons-clock',
				'capability_type'       => 'page',
				'hierarchical'          => false,
				'supports'              => array( 'title', 'excerpt', 'editor', 'thumbnail', 'revisions' ),
				'taxonomies'            => array(),
				'has_archive'           => true, //*
				'rewrite'               => array('slug' => 'clases'), //*
				'show_in_rest'          => true,
			)
		);


		//Event CTP
		register_post_type( 'event',
			array(
				$labels = array(
					'name' => __( 'Events', 'satya_plugin' ),
					'singular_name' => __( 'Event', 'satya_plugin' ),
					'add_new_item' => __( 'Add New Event', 'satya_plugin' ),
					'edit_item' => __( 'Edit Event', 'satya_plugin' ),
					'new_item' => __( 'New Event', 'satya_plugin' ),
					'view_item' => __( 'View Event', 'satya_plugin' ),
					'view_items' => __( 'View Events', 'satya_plugin' ),
					'search_items' => __( 'Search Events', 'satya_plugin' ),
					'not_found' => __( 'No Events found', 'satya_plugin' ),
					'not_found_in_trash' => __( 'No Events found in Trash', 'satya_plugin' )
				),

				'label'                 => __( 'Events', 'satya_plugin' ),
				'labels'                => $labels,
				'public'                => true,
				'exclude_from_search'   => false, //*
				'publicly_queryable'    => true, //*
				'show_ui'               => true,
				'show_in_nav_menus'     => true, //*
				'show_in_menu'          => true,
				'show_in_admin_bar'     => true,
				'menu_position'         => 20,
				'menu_icon'             => 'dashicons-calendar',
				'capability_type'       => 'page',
				'hierarchical'          => false,
				'supports'              => array( 'title', 'excerpt', 'editor', 'thumbnail', 'revisions' ),
				'taxonomies'            => array(),
				'has_archive'           => true, //*
				'rewrite'               => array('slug' => 'eventos'),
				'show_in_rest'          => true,
			)
		);


		// Therapy CTP
		register_post_type( 'therapy',
			array(
				$labels = array(
					'name' => __( 'Therapies', 'satya_plugin' ),
					'singular_name' => __( 'Therapy', 'satya_plugin' ),
					'add_new_item' => __( 'Add New Therapy', 'satya_plugin' ),
					'edit_item' => __( 'Edit Therapy', 'satya_plugin' ),
					'new_item' => __( 'New Therapy', 'satya_plugin' ),
					'view_item' => __( 'View Therapy', 'satya_plugin' ),
					'view_items' => __( 'View Therapies', 'satya_plugin' ),
					'search_items' => __( 'Search Therapies', 'satya_plugin' ),
					'not_found' => __( 'No Therapies found', 'satya_plugin' ),
					'not_found_in_trash' => __( 'No Therapies found in Trash', 'satya_plugin' )
				),

				'label'                 => __( 'Therapies', 'satya_plugin' ),
				'labels'                => $labels,
				'public'                => true,
				'exclude_from_search'   => false, //*
				'publicly_queryable'    => true, //*
				'show_ui'               => true,
				'show_in_nav_menus'     => true, //*
				'show_in_menu'          => true,
				'show_in_admin_bar'     => true,
				'menu_position'         => 20,
				'menu_icon'             => 'dashicons-heart',
				'capability_type'       => 'page',
				'hierarchical'          => false,
				'supports'              => array( 'title', 'excerpt', 'editor', 'thumbnail', 'revisions' ),
				'taxonomies'            => array('satya_therapist'),
				'has_archive'           => false, //*
				'rewrite'               => array('slug' => 'terapias'),
				'show_in_rest'          => true,
			)
		);


		// People CTP
		register_post_type( 'person',
			array(
				$labels = array(
					'name' => __( 'Person', 'satya_plugin' ),
					'singular_name' => __( 'Person', 'satya_plugin' ),
					'add_new_item' => __( 'Add New Person', 'satya_plugin' ),
					'edit_item' => __( 'Edit Person', 'satya_plugin' ),
					'new_item' => __( 'New Person', 'satya_plugin' ),
					'view_item' => __( 'View Person', 'satya_plugin' ),
					'view_items' => __( 'View People', 'satya_plugin' ),
					'search_items' => __( 'Search People', 'satya_plugin' ),
					'not_found' => __( 'No People found', 'satya_plugin' ),
					'not_found_in_trash' => __( 'No People found in Trash', 'satya_plugin' )
				),

				'label'                 => __( 'People', 'satya_plugin' ),
				'labels'                => $labels,
				'public'                => true,
				'exclude_from_search'   => false, //*
				'publicly_queryable'    => true, //*
				'show_ui'               => true,
				'show_in_nav_menus'     => true, //*
				'show_in_menu'          => true,
				'show_in_admin_bar'     => true,
				'menu_position'         => 20,
				'menu_icon'             => 'dashicons-admin-users',
				'capability_type'       => 'page',
				'hierarchical'          => false,
				'supports'              => array( 'title', 'excerpt', 'editor', 'thumbnail', 'revisions' ),
				'taxonomies'            => array( 'satya_role' ),
				'has_archive'           => false, //*
				'rewrite'               => array('slug' => 'perfil'),
				'show_in_rest'          => true,
			)
		);
	}
	add_action( 'init', 'satya_register_cpt', 0 );

}