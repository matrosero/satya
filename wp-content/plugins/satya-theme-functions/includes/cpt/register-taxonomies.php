<?php

/**
 * Register taxonomies
 *
 * @since 0.1.0
 */

if ( ! function_exists( 'satya_register_taxonomies' ) ) {

	// Register Custom Taxonomy
	function satya_register_taxonomies() {


		$labels = array(
			'name'                       => _x( 'Roles', 'Taxonomy General Name', 'satya_plugin' ),
			'singular_name'              => _x( 'Role', 'Taxonomy Singular Name', 'satya_plugin' ),
			'menu_name'                  => __( 'Role', 'satya_plugin' ),
			'all_items'                  => __( 'All Roles', 'satya_plugin' ),
			'parent_item'                => __( 'Parent Role', 'satya_plugin' ),
			'parent_item_colon'          => __( 'Parent Role:', 'satya_plugin' ),
			'new_item_name'              => __( 'New Role Name', 'satya_plugin' ),
			'add_new_item'               => __( 'Add New Role', 'satya_plugin' ),
			'edit_item'                  => __( 'Edit Role', 'satya_plugin' ),
			'update_item'                => __( 'Update Role', 'satya_plugin' ),
			'view_item'                  => __( 'View Role', 'satya_plugin' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'satya_plugin' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'satya_plugin' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'satya_plugin' ),
			'popular_items'              => __( 'Popular Roles', 'satya_plugin' ),
			'search_items'               => __( 'Search Roles', 'satya_plugin' ),
			'not_found'                  => __( 'Not Found', 'satya_plugin' ),
			'no_terms'                   => __( 'No items', 'satya_plugin' ),
			'items_list'                 => __( 'Roles list', 'satya_plugin' ),
			'items_list_navigation'      => __( 'Roles list navigation', 'satya_plugin' ),
		);
		$rewrite = array(
			'slug'                       => 'rol',
			'with_front'                 => false,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true, //change?
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
			'rewrite'                    => $rewrite,
			'show_in_rest'               => true,
		);
		register_taxonomy( 'satya_role', array( 'person' ), $args );


		$labels = array(
			'name'                       => _x( 'Therapists', 'Taxonomy General Name', 'satya_plugin' ),
			'singular_name'              => _x( 'Therapist', 'Taxonomy Singular Name', 'satya_plugin' ),
			'menu_name'                  => __( 'Therapist', 'satya_plugin' ),
			'all_items'                  => __( 'All Therapists', 'satya_plugin' ),
			'parent_item'                => __( 'Parent Therapist', 'satya_plugin' ),
			'parent_item_colon'          => __( 'Parent Therapist:', 'satya_plugin' ),
			'new_item_name'              => __( 'New Therapist Name', 'satya_plugin' ),
			'add_new_item'               => __( 'Add New Therapist', 'satya_plugin' ),
			'edit_item'                  => __( 'Edit Therapist', 'satya_plugin' ),
			'update_item'                => __( 'Update Therapist', 'satya_plugin' ),
			'view_item'                  => __( 'View Therapist', 'satya_plugin' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'satya_plugin' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'satya_plugin' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'satya_plugin' ),
			'popular_items'              => __( 'Popular Therapists', 'satya_plugin' ),
			'search_items'               => __( 'Search Therapists', 'satya_plugin' ),
			'not_found'                  => __( 'Not Found', 'satya_plugin' ),
			'no_terms'                   => __( 'No items', 'satya_plugin' ),
			'items_list'                 => __( 'Therapists list', 'satya_plugin' ),
			'items_list_navigation'      => __( 'Therapists list navigation', 'satya_plugin' ),
		);
		$rewrite = array(
			'slug'                       => 'therapist',
			'with_front'                 => false,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true, //change?
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => false,
			'show_tagcloud'              => false,
			'rewrite'                    => $rewrite,
			'show_in_rest'               => true,
		);
		register_taxonomy( 'satya_therapist', array( 'therapy' ), $args );



		$labels = array(
			'name'                       => _x( 'Types', 'Taxonomy General Name', 'satya_plugin' ),
			'singular_name'              => _x( 'Type', 'Taxonomy Singular Name', 'satya_plugin' ),
			'menu_name'                  => __( 'Type', 'satya_plugin' ),
			'all_items'                  => __( 'All Types', 'satya_plugin' ),
			'parent_item'                => __( 'Parent Type', 'satya_plugin' ),
			'parent_item_colon'          => __( 'Parent Type:', 'satya_plugin' ),
			'new_item_name'              => __( 'New Type Name', 'satya_plugin' ),
			'add_new_item'               => __( 'Add New Type', 'satya_plugin' ),
			'edit_item'                  => __( 'Edit Type', 'satya_plugin' ),
			'update_item'                => __( 'Update Type', 'satya_plugin' ),
			'view_item'                  => __( 'View Type', 'satya_plugin' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'satya_plugin' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'satya_plugin' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'satya_plugin' ),
			'popular_items'              => __( 'Popular Types', 'satya_plugin' ),
			'search_items'               => __( 'Search Types', 'satya_plugin' ),
			'not_found'                  => __( 'Not Found', 'satya_plugin' ),
			'no_terms'                   => __( 'No items', 'satya_plugin' ),
			'items_list'                 => __( 'Types list', 'satya_plugin' ),
			'items_list_navigation'      => __( 'Types list navigation', 'satya_plugin' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true, //change?
			'publicly_queryable'		 => false,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => false,
			'show_tagcloud'              => false,
			'rewrite'                    => false,
			'show_in_rest'               => true,
		);
		register_taxonomy( 'class_type', array( 'class' ), $args );


		$labels = array(
			'name'                       => _x( 'Permanences', 'Taxonomy General Name', 'satya_plugin' ),
			'singular_name'              => _x( 'Permanence', 'Taxonomy Singular Name', 'satya_plugin' ),
			'menu_name'                  => __( 'Permanence', 'satya_plugin' ),
			'all_items'                  => __( 'All Permanences', 'satya_plugin' ),
			'parent_item'                => __( 'Parent Permanence', 'satya_plugin' ),
			'parent_item_colon'          => __( 'Parent Permanence:', 'satya_plugin' ),
			'new_item_name'              => __( 'New Permanence Name', 'satya_plugin' ),
			'add_new_item'               => __( 'Add New Permanence', 'satya_plugin' ),
			'edit_item'                  => __( 'Edit Permanence', 'satya_plugin' ),
			'update_item'                => __( 'Update Permanence', 'satya_plugin' ),
			'view_item'                  => __( 'View Permanence', 'satya_plugin' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'satya_plugin' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'satya_plugin' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'satya_plugin' ),
			'popular_items'              => __( 'Popular Permanences', 'satya_plugin' ),
			'search_items'               => __( 'Search Permanences', 'satya_plugin' ),
			'not_found'                  => __( 'Not Found', 'satya_plugin' ),
			'no_terms'                   => __( 'No items', 'satya_plugin' ),
			'items_list'                 => __( 'Permanences list', 'satya_plugin' ),
			'items_list_navigation'      => __( 'Permanences list navigation', 'satya_plugin' ),
		);
		$rewrite = array(
			'slug'                       => 'permanencia',
			'with_front'                 => false,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true, //change?
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
			'rewrite'                    => $rewrite,
			'show_in_rest'               => true,
		);
		register_taxonomy( 'permanence', array( 'event' ), $args );
	}
	add_action( 'init', 'satya_register_taxonomies', 0 );

}