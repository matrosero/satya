<?php

// Save ACF custom field to date-time post
function satya_acf_save_post( $post_id ) {
    
    $acfDate = get_field('event_date', $post_id);
    
    // THIS
    $post_excerpt   = get_field( 'custom_excerpt', $post_id ); // ACF field

    $post_date = date("Y-m-d H:i:s", strtotime($acfDate));
    //Test if you receive the data field correctly:
    //echo $acfDate;
    //exit (-1);

    if ( ( $post_id ) && ( $post_excerpt || $post_date ) ) {

        $my_post = array();
        $my_post['ID'] = $post_id;
        

        if ( get_post_type( $post_id ) == 'event' && $post_date ) {
            $my_post['post_date'] = $post_date;
        }

        if ( $post_excerpt ) {
            $my_post['post_excerpt'] = $post_excerpt;
        }


        remove_action('save_post', 'satya_acf_save_post', 50); // Unhook this function so it doesn't loop infinitely

        wp_update_post( $my_post );

        add_action( 'save_post', 'satya_acf_save_post', 50); // Re-hook this function
        
    }
}
add_action('acf/save_post', 'satya_acf_save_post', 20);


function satya_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyAQPq7wGURbKFVOG5KGLiA8wH7i0A1cGHY ');
}

add_action('acf/init', 'satya_acf_init');