<?php
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> __( 'Satya Contact Settings', 'satya_plugin' ),
		'menu_title'	=> __( 'Satya Contacto', 'satya_plugin' ),
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'icon_url' => 'dashicons-admin-generic',
        'position' => 3
	));
	
	acf_add_options_page(array(
		'page_title' 	=> __( 'Schedule Settings', 'satya_plugin' ),
		'menu_title'	=> __( 'Schedule', 'satya_plugin' ),
		'menu_slug' 	=> 'satya-schedule-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'icon_url' => 'dashicons-clock',
        'position' => 20
	));
	
}