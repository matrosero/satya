<?php

add_action('acf/init', 'satya_acf_init_blocks');
function satya_acf_init_blocks() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register a testimonial block
		acf_register_block(array(
			'name'				=> 'therapies',
			'title'				=> __('Therapies', 'satya_plugin'),
			'description'		=> __('Show therapies block.', 'satya_plugin'),
			'render_callback'	=> 'satya_acf_block_render_callback',
			'category'			=> 'widgets',
			'icon'				=> 'editor-table',
			'keywords'			=> array( 'therapies', 'custom post types', 'grid' ),
		));

		// register a testimonial block
		acf_register_block(array(
			'name'				=> 'featured-text',
			'title'				=> __('Satya featured text', 'satya_plugin'),
			'description'		=> __('Full width larger text with shape', 'satya_plugin'),
			'render_callback'	=> 'satya_acf_block_render_callback',
			'category'			=> 'common',
			'icon'				=> 'format-quote',
			'keywords'			=> array( 'shapes', 'custom post types', 'grid' ),
		));

		// register an icon and text block
		// acf_register_block(array(
		// 	'name'				=> 'icon-text',
		// 	'title'				=> __('Satya icon & text', 'satya_plugin'),
		// 	'description'		=> __('Icon with text', 'satya_plugin'),
		// 	'render_callback'	=> 'satya_acf_block_render_callback',
		// 	'category'			=> 'common',
		// 	'icon'				=> 'format-quote',
		// 	'keywords'			=> array( 'shapes', 'custom post types', 'grid' ),
		// ));
	}
}

function satya_acf_block_render_callback( $block ) {
	
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/block/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/block/content-{$slug}.php") );
	}
}