<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */
?>

<?php
get_header();

get_template_part( 'template-parts/header/hero' );

do_action('satya_home_main_section');
?>

<?php
//get_sidebar();
get_footer();
