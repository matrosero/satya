<?php
/**
* The template for displaying archive pages
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package satya
*/

get_header();
?>

<!-- <div class="content-area"> -->

	<?php if ( have_posts() ) : ?>

		<header class="page-header simple-header">
			<h1 class="page-title"><?php _e('Events & workshops', 'satya'); ?></h1>
			<?php
			// the_archive_description( '<div class="archive-description">', '</div>' );
			get_template_part( 'template-parts/aside/classes','next' );
			?>
		</header><!-- .page-header -->

		<?php
		// echo '<pre>';
		// var_dump($wp_query);
		// echo '</pre>';
		?>

		<section class="events-grid">

			<?php
			$do_not_duplicate = array();

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				if ( satya_is_future() ) {
					$do_not_duplicate[] = $post->ID;
					get_template_part( 'template-parts/content/'.get_post_type(), 'loop' );
				}

			endwhile; 

			wp_reset_query();



			while ( have_posts() ) :
				the_post();

				// if ( has_term( 'permanente', 'permanence' ) )

				if ( in_array( $post->ID, $do_not_duplicate ) || !has_term( 'permanente', 'permanence' ) ) continue;

				get_template_part( 'template-parts/content/'.get_post_type(), 'loop' );
				

			endwhile;

			wp_reset_postdata(); ?>

		</section>

		<?php

		//the_posts_navigation();

	else :

		get_template_part( 'template-parts/content', 'none' );

	endif;
	?>

<!-- </div> --><!-- #primary -->

<?php
get_sidebar();
get_footer();
