<?php
/**
 * The template for displaying all single therapy posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package satya
 */

get_header();

// satya_toc_the_content_filter();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	get_template_part( 'template-parts/header/simple' ); 

	while ( have_posts() ) :
		the_post();

		//get_template_part( 'template-parts/content/therapy', 'intro' ); 

		get_template_part( 'template-parts/content/'.get_post_type(), 'single' );

	endwhile; // End of the loop.
	?>

</article><!-- #post-<?php the_ID(); ?> -->

<?php
get_footer();
