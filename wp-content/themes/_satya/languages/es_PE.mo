��    p      �  �         p	     q	     w	     �	     �	  ^   �	     �	  	   
     
     
     '
  =   <
     z
  (   
     �
     �
     �
     �
  /   �
     
       ,   4     a     h     u     |     �     �     �  4   �  	   �       _     W   ~  =   �  
             $  	   +     5     C     X     m  !         �  @   �                 	   3  !   =     _  %   f     �     �     �     �  	   �     �  F   �          &     /     8     J     `     u     �     �     �     �     �     �     �  \   �     R     b     i     u  	   z     �     �     �  ,   �     �     �  3   
     >     G     M  )   c     �     �     �  =   �     �  	   �     �     
            U   '     }  	   �     �     �     �     �     �     �     
  	   !  �  +     �     �  	   �     �  n   �  &   H  
   o     z  	   �      �  C   �     �  -   �     ,     2     9  '   F  1   n     �     �  &   �     �     �               1     I  -   ^  4   �  
   �     �  Q   �  f   8  A   �  
   �     �     �  	   �               *     D  &   a  &   �  G   �     �            	   =  &   G  	   n  '   x  	   �     �     �     �  	   �     �  J   �     :     K     M     U     r     �     �     �     �     �     �               -  �   G     �     �     �     �     �               '  /   ;     k     �  $   �     �     �  $   �  /   �     $     +     :  O   @     �  
   �     �     �     �  
   �  R   �     0   
   8      C      Z      u      �   
   �      �      �      �      )   Y              G      L       V      n       W   ^      H   "   K       +   f      M   b   m      5           ]       U           (           	   d   Z   k   4   O   *   .          i       j   1      8   _   S          2   g   p   T   l         X   P                  A      ,   0   -   R   $                  N       '   
             :      #              &                    o   I   ?      3      7       @       !   ;   J          h       E                         =   %   [   B       \   9   c   a   /   Q   e   C   6      >   F   D          <   `         and   in %1$s %s icon ,  <span id="footer-thankyou">Developed by <a href="#" target="_blank">Your Site Name</a></span>. Add widgets to the home page. Cancelled Choose Image Circles Comments are closed. Continue reading<span class="screen-reader-text"> "%s"</span> Cost Custom RSS Feed (Customize in admin.php) Date Dates Description Displays events in a grid. Edit <span class="screen-reader-text">%s</span> Events & workshops Events &amp; workshops Featured, larger text with decorative shape. Flower Footer Links Friday Full moon, no classes Get in touch! Holistic therapies Home Content Section I want to be notified of future workshop and events. In person Instructor Instructors It looks like nothing was found at this location. Maybe try one of the links below or a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a Comment<span class="screen-reader-text"> on %s</span> Mat Rosero Menu Monday More %1$s More articles Most Used Categories New moon, no classes Next Class Notice Next full moon: <time>%1$s</time> Next new moon: <time>%1$s</time> No classes today due to the %2$s. <a href="/%1$s">Learn why.</a> No shape Nothing Found Number of events to show Offcanvas One thought on &ldquo;%1$s&rdquo; Online Oops! That page can&rsquo;t be found. Pages: Posts by %s Primary Menu Read  Read more Read more about %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Regular classes RemoveX Saturday Satya Events Grid Satya Text with Shape Satya Therapies Grid Search Results for: %s See all classes Select Select  Select Image Show next class notice Skip to content Social Media Accounts Sorry, but nothing matched your search terms. Please try again with some different keywords. Special classes Sunday Tagged %1$s Text Textarea: The Main Menu The Mobile Menu The Off-Canvas Menu The RSS Feed is either empty or unavailable. The Tablet Menu The offcanvas sidebar. There's no upcoming dates scheduled for this event. Thursday Title Title as part of grid Try looking in the monthly archives. %1$s Tuesday Upcoming class Upload Use the links below to access the different content sections. Venue Wednesday Where to find us Widget Title _Satya articles comments title%1$s thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; events full moon http://underscores.me/ https://matilderosero.com/ j F Y @ g:i a list item separator,  new moon post authorPublished by %s post datePosted on %s therapies Project-Id-Version: _Satya
PO-Revision-Date: 2023-05-12 16:01-0600
Last-Translator: 
Language-Team: 
Language: es_PE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: style.css
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
  y   en %1$s %s ícono ,  <span id=“footer-thankyou”>Desarrollado por <a href=“#” target=“_blank”>Your Site Name</a></span>. Añada widgets a la página de inicio. Suspendida Elijir imagen Círculos Los comentarios están cerrados. Seguir leyendo<span class=“screen-reader-text”> “%s”</span> Precio RSS personalizado (personalizar en admin.php) Fecha Fechas Descripción Mostrar los eventos en una cuadrícula. Editar <span class="screen-reader-text">%s</span> Eventos y talleres Eventos &amp; talleres Texto resaltado con imagen decorativa. Flor Enlaces de pie de pagina Viernes Luna llena, no hay clases ¡Póngase en contacto! Terapias holísticas Sección de contenido de la página de inicio Quiero ser notificado de futuros talleres y eventos. Presencial Facilitador Facilitadores Parece que no hay nada aquí. Pruebe uno de los enlaces de abajo o una búsqueda? Parece que no&rsquo;podemos encontrar lo que&rsquo;estás buscando. Tal vez la búsqueda puede ayudar. Deje un comentario<span class="screen-reader-text"> en %s </span> Mat Rosero Menú Lunes Más %1$s Más artículos Categorías más usadas Luna nueva, no hay clases Notificación próxima clase Próxima luna llena: <time>%1$s</time> Próxima luna nueva: <time>%1$s</time> Hoy no hay clases por la %2$s. <a href=“%1$s”>Aprenda por qué.</a> Ninguna imagen No se ha encontrado nada Cantidad de eventos a mostrar Offcanvas Un comentario sobre &ldquo;%1$s&rdquo; En línea Oops! No pudimos encontrar esa página. Páginas: Publicado por %s Menú Primario Leer  Leer más Leer más sobre %1$s ¿Listo para publicar tu primer post? <a href="%1$s">Get started here</a>. Clases regulares X Sábado Cuadrícula de eventos Satya Satya Texto con Imagen Cuadrícula de terapias Satya Buscar resultados para: %s Ver todas las clases Seleccionar Seleccionar  Seleccionar imagen Mostrar notificación Saltar al contenido Cuentas de redes sociales Lo sentimos, pero nada coincide con sus términos de búsqueda. Por favor, inténtelo de nuevo con algunas palabras clave diferentes. Clases especiales Domingo Etiquetado %1$s Texto Texto: El menú principal El menú móvil El menú OFF-Canvas El RSS Feed está vacío o no está disponible. El menú para tabletas Barra lateral offcanvas. No hay fechas próximas programadas. Jueves Título Título como parte de la cuadrícula Trate de buscar en los archivos mensuales. %1$s Martes Próxima clase Subir Puede usar estos enlaces para navegar a las diferentes secciones del contenido. Lugar Miércoles Dónde nos encontramos Título del Widget _Satya artículos %1$s comentario sobre &ldquo;%2$s&rdquo; %1$s comentarios sobre &ldquo;%2$s&rdquo; eventos luna llena http://underscores.me/ https://matilderosero.com/ j F Y @ g:i a ,  luna nueva Publicado por %s Publicado el %s terapias 