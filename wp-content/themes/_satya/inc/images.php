<?php

/**
 * Responsive Image Helper Function
 *
 * @param string $image_id the id of the image (from ACF or similar)
 * @param string $image_size the size of the thumbnail image or custom image size
 * @param string $max_width the max width this image will be shown to build the sizes attribute 
 * http://aaronrutley.com/responsive-images-in-wordpress-with-acf/
 *
 * Returns:
 * '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px'
 */
function satya_build_srcset_sizes($small = null, $medium = null, $large = null, $xlarge = null) {

    $sizes = '';

    if ( isset( $small ) ) {
        // $sizes['640px'] = $small;
        $sizes .= '(max-width: 640px) '.$small.', ';
    }

    if ( isset( $medium ) ) {
        // $sizes['1024px'] = $medium;
        $sizes .= '(max-width: 960px) '.$medium.', ';
    }

    if ( isset( $large ) ) {
        // $sizes['1200px'] = $large;
        if ( isset( $xlarge ) ) {
            $sizes .= '(max-width: 1290px) '.$large.', ';
        } else {
            // $sizes['1200px'] = $large;
            $sizes .= $large;
        }

    }

    if ( isset( $xlarge ) ) {
        $sizes .= $xlarge;
    }

    return $sizes;

}

function satya_srcset($image_id,$image_size,$sizes,$class=null){

    // check the image ID is not blank
    if($image_id != '') {

        // var_dump($image_size);
        $src = wp_get_attachment_image_src( $image_id, $image_size );
        // var_dump($src);
        $srcset = wp_get_attachment_image_srcset( $image_id, $image_size );
        // $sizes = wp_get_attachment_image_sizes( $image_id, $image_size );
        $alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true);

        $return = '<img src="' . esc_attr( $src[0] ) . '"
            srcset="' . esc_attr( $srcset ) . '"
            sizes="' . esc_attr( $sizes ) . '"
            alt="' . esc_attr( $alt ) . '"';

        if ( $class ) {
            $return .= ' class="'.$class.'"';
        }

        $return .= ' />';

        return $return;

    }
}

function satya_simple_srcset( $img ) {
  $img_id = attachment_url_to_postid( $img );
  $img_srcset = wp_get_attachment_image_srcset( $img_id );
  $img_sizes = wp_get_attachment_image_sizes( $img_id, array('1980') );
  $alt = '';
  $alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
  return '<img src="' . $img . '" srcset="' . esc_attr( $img_srcset ) . '" sizes="' . esc_attr( $img_sizes ) . '" alt="' . esc_attr( $alt ) . '" />';
}