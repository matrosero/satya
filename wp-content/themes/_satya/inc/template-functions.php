<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package satya
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function satya_body_classes( $classes ) {
	global $post;

    $classes[] = 'no-js';

    // Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

    if (isset($post->post_content)) {

        $content = $post->post_content;


        if ( get_post_meta( $post->ID, 'show_toc', true ) ||  has_shortcode( $content, 'indice-contenidos' ) ) {
            $classes[] = 'has-toc';
        }
    
        if ( ( ( is_home() || is_front_page() ) && get_header_image() ) ||has_post_thumbnail( $post->ID ) ) {
            $classes[] = 'has-hero-image';
        }

    }

	return $classes;
}
add_filter( 'body_class', 'satya_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function satya_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'satya_pingback_header' );


function satya_pre_get_posts( $query ) {
    if ( is_admin() || ! $query->is_main_query() )
        return;

    if ( is_post_type_archive( 'event' ) ) {
        // Display 50 posts for a custom post type called 'movie'
        // $query->set( 'posts_per_page', 6 );

        $query->set('order', 'ASC');
        $query->set('posts_per_page', -1);

        // $meta_query = array(
        //     array(
        //         'key' => 'event_date_end',
        //         'value' => date('Y-m-d'),
        //         'compare' => '<=',
        //         'type' => 'NUMERIC'
        //      )
        // );
        // $query->set('meta_query',$meta_query);
        
        // $query->set(
        //     'date_query', 
        //     array(
        //         array(
        //             'after' => date('Y-m-d'),
        //             'inclusive' => true,
        //         ),
        //     ));
    }

    return;
}
add_action( 'pre_get_posts', 'satya_pre_get_posts', 1 );


function satya_show_therapies_grid($query = null, $classes = null) {
    if ( !$query ) {
        return false;
    }
    ob_start();
  
    if ($query->have_posts()) :

        if ( $classes ) {
            $classes .= ' therapies-grid'; 
        } else {
            $classes = ' therapies-grid';
        } ?>

        <section class="therapies-grid <?php echo $classes; ?>">

            <?php
            while ($query->have_posts()) : $query->the_post();
                get_template_part( 'template-parts/content/therapy', 'loop' );
            endwhile;
            ?>

        </section>
            
    <?php 
    endif;
    wp_reset_postdata();

    $output = ob_get_clean();

    return $output;
    
}


function satya_query_therapies_by_person( $post_id = null ) {
    // var_dump($post_id);
    global $post;

    if ( !$post_id ) {
        $post_id = $post->ID;
    }
    
    /*
         * The WordPress Query class.
         *
         * @link http://codex.wordpress.org/Function_Reference/WP_Query
         */
        $args = array(
    
            // Type & Status Parameters
            'post_type'   => 'therapy',
            'post_status' => 'publish',
    
    
            // Order & Orderby Parameters
            'order'               => 'ASC',
            'orderby'             => 'title',
    
            // Pagination Parameters
            'posts_per_page'         => -1,
    
            // Custom Field Parameters
            'meta_query'     => array(
                array(
                    'key'     => 'therapist',
                    'value'   => '"' . $post_id . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
                    'compare' => 'LIKE',
                ),
            ),
    
            // Permission Parameters -
            'perm' => 'readable',
    
            // Parameters relating to caching
            'no_found_rows'          => false,
            'cache_results'          => true,
            'update_post_term_cache' => true,
            'update_post_meta_cache' => true,
    
        );
    
    $query = new WP_Query( $args );

    if ( $query->have_posts() ) {
        return $query;
    }
    
}


function satya_weekdays() {
	$days = array(
        array( 1, __('Monday', 'satya') ),
		array( 2, __('Tuesday', 'satya') ),
		array( 3, __('Wednesday', 'satya') ),
		array( 4, __('Thursday', 'satya') ),
		array( 5, __('Friday', 'satya') ),
		array( 6, __('Saturday', 'satya') ),
	);
	return $days;
}

function satya_moondays_option_names() {
	$days = array(
		'options_daily_moon',
		'options_tuesday_daily_moon',
		'options_wednesday_daily_moon',
		'options_thursday_daily_moon',
		'options_friday_daily_moon',
		'options_saturday_daily_moon',
	);
	return $days;
}

function satya_weekday_option_names() {
	$days = array(
		'options_daily_classes',
		'options_tuesday_daily_classes',
		'options_wednesday_daily_classes',
		'options_thursday_daily_classes',
		'options_friday_daily_classes',
		'options_saturday_daily_classes',
	);
	return $days;
}


/**
 *  Returns the blog timezone
 *
 * Gets timezone settings from the db. If a timezone identifier is used just turns
 * it into a DateTimeZone. If an offset is used, it tries to find a suitable timezone.
 * If all else fails it uses UTC.
 *
 * @return DateTimeZone The blog timezone
 */
function satya_get_blog_timezone() {

    $tzstring = get_option( 'timezone_string' );
    $offset   = get_option( 'gmt_offset' );

    //Manual offset...
    //@see http://us.php.net/manual/en/timezones.others.php
    //@see https://bugs.php.net/bug.php?id=45543
    //@see https://bugs.php.net/bug.php?id=45528
    //IANA timezone database that provides PHP's timezone support uses POSIX (i.e. reversed) style signs
    if( empty( $tzstring ) && 0 != $offset && floor( $offset ) == $offset ){
        $offset_st = $offset > 0 ? "-$offset" : '+'.absint( $offset );
        $tzstring  = 'Etc/GMT'.$offset_st;
    }

    //Issue with the timezone selected, set to 'UTC'
    if( empty( $tzstring ) ){
        $tzstring = 'UTC';
    }

    $timezone = new DateTimeZone( $tzstring );
    return $timezone; 
}


function mro_print_scripts_styles() {

    $result = [];
    $result['scripts'] = [];
    $result['styles'] = [];

    // Print all loaded Scripts
    global $wp_scripts;
    foreach( $wp_scripts->queue as $script ) :
       $result['scripts'][] =  $wp_scripts->registered[$script]->src . ";";
    endforeach;

    // Print all loaded Styles (CSS)
    global $wp_styles;
    foreach( $wp_styles->queue as $style ) :
       $result['styles'][] =  array(
       		'name' => $style,
       		'src' => $wp_styles->registered[$style]->src . ";",
       	);
    endforeach;

    // return $result;
    echo '<pre>';
    var_dump($result);
    echo '</pre>';
}


function satya_manual_excerpt_more( $excerpt ) {
    $excerpt_more = '';
    if( has_excerpt() ) {
        
        if ( 'class' == get_post_type() ) {
            $excerpt_more = '&nbsp;<p class="more-link"><a href="' . get_permalink() . '" class="button" rel="nofollow">'.sprintf( esc_html__( 'Read more about %s', 'satya' ), get_the_title() ).'</a></';
        }
        
    }
    return $excerpt . $excerpt_more;
}
add_filter( 'get_the_excerpt', 'satya_manual_excerpt_more' );


add_filter( 'pll_custom_flag', 'pll_custom_flag', 10, 2 );
 
function pll_custom_flag( $flag, $code ) {
    $flag['url']    = get_stylesheet_directory_uri()."/assets/polylang/4x3/{$code}.svg";
    $flag['width']  = 32;
    $flag['height'] = 24;
    return $flag;
}