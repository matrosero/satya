<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package satya
 */

function satya_get_phone_hours() {
	if ( get_option( 'options_contact_phone_schedule_start' ) && get_option( 'options_contact_phone_schedule_end' ) ) {

		$start = get_option( 'options_contact_phone_schedule_start' );
		$end = get_option( 'options_contact_phone_schedule_end' );

		$start  = date("g:i a", strtotime($start));
		$end  = date("g:i a", strtotime($end));

		return '<p>Horario de atención:<br />
			De '.$start.' a '.$end.'</p>';
	}
}

function satya_get_phone($person_id = null) {
	
	if ( ! get_option( 'options_contact_phone' ) && !$person_id ) {
		return false;
	}

	if ( $person_id ) {
		$phone = esc_html( get_post_meta( $person_id, 'person_phone', 1 ) );
	
	} elseif ( get_option( 'options_contact_phone' ) ) { 

		$phone = esc_html( get_option( 'options_contact_phone' ) ); 
	}

	// if ( preg_match( '/^(\+?\d{1,3}?)[ .-]?(?(\d{4}))?[ .-]?(\d{4})[ .-]?(\d{4})$/', $phone,  $matches ) ) {
	//     $result = $matches[1] . '-' .$matches[2] . '-' . $matches[3];
	//     echo $result;
	// }
	
	return '<span class="phone"><a href="tel:'.$phone.'">'.$phone.'</a></span>';
}

if ( ! function_exists( 'satya_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function satya_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( DATE_W3C ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( DATE_W3C ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			/* translators: %s: post date. */
			esc_html_x( 'Posted on %s', 'post date', 'satya' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span>'; // WPCS: XSS OK.

	}
endif;

if ( ! function_exists( 'satya_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function satya_posted_by() {
		$byline = sprintf(
			/* translators: %s: post author. */
			esc_html_x( 'Published by %s', 'post author', 'satya' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

	}
endif;


if ( ! function_exists( 'satya_posted_in__header' ) ) :
	function satya_posted_in__header() {
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( esc_html__( ', ', 'satya' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				printf( '<span class="cat-links">' . esc_html__( ' in %1$s', 'satya' ) . '</span>', $categories_list ); // WPCS: XSS OK.
			}
		}
	}
endif;

if ( ! function_exists( 'satya_tags' ) ) :
	function satya_tags() {
		if ( 'post' === get_post_type() ) {
			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'satya' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'satya' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}
	}
endif;



if ( ! function_exists( 'satya_entry_footer' ) ) :
	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 */
	function satya_entry_footer() {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {

			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'satya' ) );
			if ( $tags_list ) {
				/* translators: 1: list of tags. */
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'satya' ) . '</span>', $tags_list ); // WPCS: XSS OK.
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link(
				sprintf(
					wp_kses(
						/* translators: %s: post title */
						__( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'satya' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'satya' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'satya_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function satya_post_thumbnail() {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<figure class="header-image">
				<?php the_post_thumbnail(); ?>
			</figure><!-- .header-image -->

		<?php else : ?>

		<a class="header-image" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
			<?php
			the_post_thumbnail( 'post-thumbnail', array(
				'alt' => the_title_attribute( array(
					'echo' => false,
				) ),
			) );
			?>
		</a>

		<?php
		endif; // End is_singular().
	}
endif;


if ( ! function_exists( 'satya_show_associated_people' ) ) :
	/**
	 * Prints list of people associated.
	 */
	function satya_show_associated_people( $people = null ) {
		if ( !$people || !is_array( $people ) ) {
			return;
		}
		if ( is_array($people) ) {
			
			foreach ($people as $key => $person) {
				
				$name = get_the_title( $person );
				$people[$key] = $name;
			}

			$last  = array_slice($people, -1);
			$first = join(', ', array_slice($people, 0, -1));
			$both  = array_filter(array_merge(array($first), $last), 'strlen');
			$return = join( __(' and ', 'satya'), $both);

			return $return;
		}
	}
endif;


if ( ! function_exists( 'satya_list_associated_people' ) ) :
	/**
	 * Prints list of people associated.
	 */
	function satya_list_associated_people( $people = null ) {
		if ( !$people || !is_array( $people ) ) {
			return;
		}
		if ( is_array($people) ) {
			
			foreach ($people as $key => $person) {
				
				$name = get_the_title( $person );
				$people[$key] = $name;
			}

			$last  = array_slice($people, -1);
			$first = join(', ', array_slice($people, 0, -1));
			$both  = array_filter(array_merge(array($first), $last), 'strlen');
			$return = join( __(' and ', 'satya'), $both);

			return $return;
		}
	}
endif;


if (!function_exists('satya_show_block_header')) :
    function satya_show_block_header($title, $link=null, $article_type='articles') {
        if ( $article_type == 'articles' ) {
            $article_type = __( 'articles', 'satya' );
        } elseif ( $article_type == 'events' ) {
            $article_type = __( 'events', 'satya' );
        } elseif ( $article_type == 'therapy' ) {
            $article_type = __( 'therapies', 'satya' );
        }
        ?>
        <header class="section-header">

            <?php if (!empty($title)): ?>
                <h2 class="section-title">
                    <?php echo esc_html($title); ?>
                </h2>
            <?php endif; ?>

            <?php if (!empty($link)): ?>
                <p><a class="more" href="<?php echo esc_url( $link ); ?>" title="<?php _e( 'More articles', 'satya' ); ?>"><?php printf( esc_html__( 'More %1$s', 'satya' ), $article_type ); ?></a></p>
            <?php endif; ?>

        </header>
    <?php }
endif;


if(!function_exists('satya_get_events')):
    function satya_get_events( $number_of_posts, $future = true, $exclude = array(), $permanent = false ) {

        $args = array(
            'post_type' => 'event',
            'posts_per_page' => absint($number_of_posts),
            'post_status' => 'publish',
            'orderby' => 'title',
            'order' => 'ASC',
        );

        if ( $future ) {
        	$args['orderby'] = 'date';
        	$args['date_query'] = array(
            	array(
            		'after'	=> date('Y-m-d'),
            		'inclusive'	=> true,
            	),
            );
        }

        if ( !empty($exclude) ) {
        	$args['post__not_in'] = $exclude;
        }

        if ( $permanent ) {
        	$args['tax_query'] = array(
				array(
					'taxonomy' => 'permanence',
					'field'    => 'slug',
					'terms'    => 'permanente',
				),
			);

        }



        $all_posts = new WP_Query($args);
        return $all_posts;
    }

endif;