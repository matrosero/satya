<?php
if( function_exists('pll_get_post_language') && function_exists('pll_the_languages') && !function_exists('satya_menu_language_switcher') ):

	function satya_menu_language_switcher($show = 'name',$class = 'menu-item menu-item-language') {
		global $post;


		$current = pll_get_post_language($post->ID, $show);

		ob_start();

		?>

		<!-- <li class="<?php echo $class; ?>"><a href="#" class="toggle-language current-language"><?php echo $current; ?></a> -->

			<!-- <ul class="language-menu"> -->

				<?php

				pll_the_languages();

				$translations = pll_the_languages(array('raw'=>1));

				foreach ($translations as $key => $translation) {
					echo '<li class="'.implode(' ',$translation['classes']).'"><a hreflang="'.$translation['locale'].'" href="'.$translation['url'].'" lang="'.$translation['locale'].'">'.$translation[$show].'</a></li>';
				} ?>

			<!-- </ul> -->

		<!-- </li> -->

		<?php

		$output = ob_get_clean();

	    return $output;
	}
endif;

add_action('init', function() {
  pll_register_string('satya-mailchimp-form-paragraph', 'Quiero recibir noticias de Ashtanga Yoga Satya');
});