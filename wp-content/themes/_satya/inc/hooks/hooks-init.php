<?php

/**
 * Front page section additions.
 */
require get_template_directory().'/inc/hooks/hook-home.php';

/**
 * Add custom fields to events content.
 */
require get_template_directory().'/inc/hooks/hook-content.php';

/**
 * Add TOC content.
 */
require get_template_directory().'/inc/hooks/hook-toc.php';

/**
 * Change title tag.
 */
require get_template_directory().'/inc/hooks/hook-titles-head.php';

/**
 * Gettext.
 */
// require get_template_directory().'/inc/hooks/hook-gettext.php';