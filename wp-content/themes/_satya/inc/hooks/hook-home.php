<?php
if ( ! function_exists( 'satya_home_main_widgets_section' ) ) :
    /**
     *
     * @since CoverNews 1.0.0
     *
     * @param null
     * @return null
     *
     */
    function satya_home_main_widgets_section() {
        ?>
        <!-- Main Content section -->
        <?php
        if ( is_active_sidebar( 'home') ) {  ?>

           <?php dynamic_sidebar('home'); ?>

        <?php }
    }
endif;
add_action( 'satya_home_main_section', 'satya_home_main_widgets_section', 50 );