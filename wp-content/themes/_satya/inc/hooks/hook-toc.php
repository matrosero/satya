<?php

function satya_build_toc($toc, $heading = null) {

	if ( !empty( $toc ) && is_array($toc) ) {

		if ( !$heading ) {
            $heading =  __('Use the links below to access the different content sections.', 'satya');
        }

        $return = '<nav class="toc-navigation">
            <p>'.$heading.'</p>
            <ul class="toc">';

		foreach ($toc as $key => $value) {

			$return .= '<li><a href="#' . $value[1] . '">' . $value[0] . '</a></li>';
		}

		$return .= '</ul></nav>';

		return $return;
	} 
}

function satya_string_to_link($string) {
	if ( $string ) {
		return strtolower(str_replace(' ', '-', sanitize_title($string)));
	}
}

add_filter( 'the_content', 'satya_toc_the_content_filter', 20 );
/**
 * Add a table of contents based on post's heading.
 *
 */
function satya_toc_the_content_filter( $content ) {

    global $post;

    if ( get_post_meta( $post->ID, 'show_toc', true ) ) {
        
    	// Array to hold TOC elements
    	$toc = array();
    	
    	$i = 0;

		// Regular expression to match headings    	
    	$regex = '/(\<h[1-6](.*?))\>(.*)(<\/h[1-6]>)/i';

    	// Match all headings
    	preg_match_all($regex, $content, $matches);

    	// Populate array with headings
    	foreach ($matches[3] as $key => $value) {
    		$toc[] = array(
    			$value,
    			satya_string_to_link($value)
    		); 
    	}

    	$content = preg_replace_callback( $regex, function( $matches ) {
			if ( ! stripos( $matches[0], 'id=' ) ) :
				$matches[0] = $matches[1] . $matches[2] . ' id="' . sanitize_title( $matches[3] ) . '">' . $matches[3] . $matches[4];
			endif;
			return $matches[0];
		}, $content );

		$content = satya_build_toc($toc) . $content;
         	
    }


    // Returns the content.
    return $content;
}