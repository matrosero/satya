<?php

add_filter( 'gettext', 'satya_dynamic_strings', 20, 3 );
/**
 * Change comment form default field names.
 *
 * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/gettext
 */
function satya_dynamic_strings( $translated_text, $text, $domain ) {
 
    if ( is_page_template('page-templates/class-schedule.php') ) {
 
        switch ( $translated_text ) {
 
            case 'inperson' :
 
                $translated_text = __( 'In person', 'satya' );
                break;
 
            case 'online' :
 
                $translated_text = __( 'Online', 'satya' );
                break;
        }
 
    }
 
    return $translated_text;
}