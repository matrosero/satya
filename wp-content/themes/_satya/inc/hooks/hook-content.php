<?php

add_filter( 'the_content', 'satya_the_content_filter', 15 );
/**
 * Add Custom Fields to the_content() in an event post.
 *
 * @uses is_single()
 */
function satya_the_content_filter( $content ) {
	
	global $post;

	if ( is_singular('therapy') && !empty(get_post_meta( $post->ID, 'therapist', true ) ) ) {
    	
    	$therapists = get_post_meta( $post->ID, 'therapist', true );

   		$args = array( 
   			'post__in' => $therapists,
   			'post_type'   => 'person',
			'post_status' => 'publish',
   		);
    	
    	$query = new WP_Query( $args );

    	ob_start();

    	if ($query->have_posts()) :
    		if ($query->post_count > 1) :
		        echo '<h2 id="profesores">Terapeutas</h2>';
		    else:
		    	echo '<h2 id="profesores">Terapeuta</h2>';
		    endif;
	        echo '<ul class="people">';
	        while ($query->have_posts()) : $query->the_post();

		    	get_template_part( 'template-parts/content/person', 'loop' );

		    endwhile;
		    echo '</ul>';
	    endif;
    
    	wp_reset_postdata();
    
		$add = ob_get_clean();

    	$content .= $add;
    	
	}
	
	if ( is_singular('therapy') ) {
    	$appointment = satya_thecontent_therapy_appoinments();
    	$content = $content.$appointment;
    }
	
	if ( is_singular('event') && !empty(get_post_meta( $post->ID, 'teacher', true ) ) ) {
    	
    	$teachers = get_post_meta( $post->ID, 'teacher', true );

   		$args = array( 
   			'post__in' => $teachers,
   			'post_type'   => 'person',
			'post_status' => 'publish',
   		);
    	
    	$query = new WP_Query( $args );

    	ob_start();

    	if ($query->have_posts()) :

			echo '<h2 id="profesores">' . _n( 'Instructor', 'Instructors', $query->post_count, 'satya' ) . '</h2>';

    		// if ($query->post_count > 1) :
		    //     echo '<h2 id="profesores">';
			// 	_e('Facilitadores', 'satya');
			// 	echo '</h2>';
		    // else:
		    // 	echo '<h2 id="profesores">';
			// 	_e('Facilitadors', 'satya');
			// 	echo '</h2>';
		    // endif;
	        echo '<ul class="people">';
	        while ($query->have_posts()) : $query->the_post();

		    	get_template_part( 'template-parts/content/person', 'loop' );

		    endwhile;
		    echo '</ul>';
	    endif;
    
    	wp_reset_postdata();
    
		$add = ob_get_clean();

    	$content = $add.$content;
    	
    }

    if ( is_singular('person') ) {
    	if ( has_post_thumbnail( $post->ID ) ) {
	    	$feat_image = '<div class="profile-image">'.get_the_post_thumbnail( $post->ID, 'full' ).'</div>';
	    	$content = $feat_image.$content;
	    }
	    if ( ! has_term( 'fundador', 'satya_role' ) && has_term( 'terapeuta', 'satya_role' ) ) {
	    	
	    	if ( $query = satya_query_therapies_by_person() ) {

	  	    	$content .= '<p>'.get_the_title( $post->ID ).' es terapeuta y ofrece sesiones en Ashtanga Yoga Satya de:</p>';

	  	    	$therapies = satya_show_therapies_grid($query);

		    	$content .= $therapies;  

		    	$appointment = satya_thecontent_therapy_appoinments( $post->ID );
	    		$content .= $appointment;		
	    	}
	    }
    }

    $content = str_replace('<p></p>', '', $content);

    // Returns the content.
    return '<div class="wrapper">'.$content.'</div>';
}


// add_filter( 'the_content', 'satya_no_empty_filter', 20 );
// /**
//  * Add Custom Fields to the_content() in an event post.
//  *
//  * @uses is_single()
//  */
// function satya_no_empty_filter( $content ) {

//     $content = str_replace('<p></p>', '', $content);

//     // Returns the content.
//     return $content;
// }



function satya_thecontent_therapy_appoinments( $therapist_id = null ) {
	
	global $post;

	if ( is_singular('person') && ( ! $therapist_id || ! get_post_meta( $therapist_id, 'person_phone', 1 ) ) ) {
		return false;
	}

	ob_start(); ?>

	<div class="schedule-appointment">
		<div class="boxy">
			<?php
			if ( is_singular('therapy') ) { 

				if ( has_term( 'satya', 'satya_therapist') && get_option( 'options_contact_phone' ) ) { ?>
					<h2> Agende su cita</h2>
					<?php 
					$contact = esc_html ( get_option( 'options_contact_therapy' ) );
					echo wpautop($contact);
					echo satya_get_phone_hours(); ?>
					<p><strong>Se darán citas únicamente por teléfono.</strong></p>

				<?php } elseif ( has_term( 'independiente', 'satya_therapist') ) { ?>

					<h2> Agende su cita directamente con:</h2>

					<ul>

						<?php 
						$therapists = get_post_meta( $post->ID, 'therapist', 1 );

						foreach ($therapists as $key => $therapist_id) {

							echo '<li><a href="'.get_the_permalink( $therapist_id ).'">'.get_the_title( $therapist_id ).'</a>, tel. '.satya_get_phone($therapist_id).'</li>';
							
						} ?>

					</ul>

				<?php }

			} elseif ( is_singular('person') && $therapist_id && get_post_meta( $therapist_id, 'person_phone', 1 ) ) { 

				$name = get_the_title( $therapist_id );
				$phone = satya_get_phone($therapist_id);

				?>

				<h2> Agende su cita al <?php echo $phone; ?></h2>

				<p>Sesiones se agendan directamente con <strong><?php echo $name; ?>.</strong></p>

			<?php } ?>
		</div>
	</div>

	<?php
	$output = ob_get_clean();

    return $output;
}