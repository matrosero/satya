<?php

// add filter
add_filter('pre_get_document_title', 'satya_change_title');
// Our function
function satya_change_title($title) {
     if ( is_post_type_archive( 'event' ) ) {
       return __('Events & workshops', 'satya');
     }
     // else return default title
     return $title;
}