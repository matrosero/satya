<?php

/* Load widget base. */
require_once get_template_directory() . '/inc/widgets/widgets-base.php';

/* Theme Widget sidebars. */
require get_template_directory() . '/inc/widgets/widgets-register-sidebars.php';

/* Theme Widget sidebars. */
require get_template_directory() . '/inc/widgets/widgets-common-functions.php';

/* Theme Widgets*/
require get_template_directory() . '/inc/widgets/widget-therapies-grid.php';
require get_template_directory() . '/inc/widgets/widget-events-grid.php';
require get_template_directory() . '/inc/widgets/widget-text-shape.php';


/* Register site widgets */
if ( ! function_exists( 'satya_widgets' ) ) :
    /**
     * Load widgets.
     *
     * @since 1.0.0
     */
    function satya_widgets() {
    	register_widget( 'Satya_Therapies_Grid' );
    	register_widget( 'Satya_Events_Grid' );
    	register_widget( 'Satya_Text_Shape' );

    }
endif;
add_action( 'widgets_init', 'satya_widgets' );