<?php

function satya_social_media_array() {

	/* store social site names in array */
	$social_sites = array('facebook', 'linkedin', 'youtube', 'twitter', 'instagram', 'whatsapp', 'rss', 'email');

	return $social_sites;
}

/* takes user input from the customizer and outputs linked social media icons */
function satya_social_media_icons() {

    $social_sites = satya_social_media_array();

    /* any inputs that aren't empty are stored in $active_sites array */
    foreach($social_sites as $social_site) {
        if( strlen( get_theme_mod( $social_site ) ) > 0 ) {
            $active_sites[] = $social_site;
        }
    }

    /* for each active social site, add it as a list item */
    if ( ! empty( $active_sites ) ) {

        echo "<ul class='social-media-icons menu'>";

        foreach ( $active_sites as $active_site ) {

            /* setup the class */
	        $class = 'icon-' . $active_site;

	        ?>

            <li>
                <a class="icon-social-link <?php echo $active_site; ?>" target="_blank" href="<?php echo esc_url( get_theme_mod( $active_site) ); ?>">
                    <i class="<?php echo esc_attr( $class ); ?>" title="<?php printf( __('%s icon', 'satya'), $active_site ); ?>"></i><span class="screen-reader-text"><?php printf( ucfirst($active_site) ); ?></span>
                </a>
            </li>
        
        <?php }

        echo "</ul>";
    }
}