<?php
/*
 * Class schedule callback
 * - [moon-next]
 *
 * Returns list of team members
 */
function satya_moon_next_shortcode($atts) {
    // global $post, $wpdb;
	date_default_timezone_set('America/Costa_Rica');

    if ( !$moon ) {
    	$moon = new Solaris\MoonPhase();
    }

	$next_new = new DateTime(gmdate('Y-m-d H:i:s', $moon->get_phase('new_moon')), new DateTimeZone('UTC'));
	$next_full = new DateTime(gmdate('Y-m-d H:i:s', $moon->get_phase('full_moon')), new DateTimeZone('UTC'));

	$today = new DateTime( null, satya_get_blog_timezone() );
	// $today = new DateTime( '2019-03-21 00:23:10.913888', satya_get_blog_timezone() );
	// var_dump($next_new);

	if ( $today > $next_new ) {
		$next_new = new DateTime(gmdate('Y-m-d H:i:s', $moon->get_phase('next_new_moon')), new DateTimeZone('UTC'));
	}

	if ( $today > $next_full ) {
		$next_full = new DateTime(gmdate('Y-m-d H:i:s', $moon->get_phase('next_full_moon')), new DateTimeZone('UTC'));
	}


	ob_start();

	?>

	<aside class="moon-next">

		<?php
		if ($next_new < $next_full) {
			echo '<p class="first">';
		} else {
			echo '<p class="last">';
		}

		printf( 
			__( 'Next new moon: <time>%1$s</time>', 'satya' ), 
			satya_timestamp_to_spanish_date( $next_new->getTimestamp() ) );
		?></p>
		
		<?php
		if ($next_full < $next_new) {
			echo '<p class="first">';
		} else {
			echo '<p class="last">';
		}
	
			printf( 
				__( 
					'Next full moon: <time>%1$s</time>', 'satya' 
				), 
				satya_timestamp_to_spanish_date( $next_full->getTimestamp() ) ); 
			?></p>

	</aside>

	<?php
    $output = ob_get_clean();

    return $output;

}