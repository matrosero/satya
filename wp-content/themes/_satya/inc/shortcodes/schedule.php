<?php
/*
 * Class schedule callback
 * - [class-schedule]
 *
 * Returns list of team members
 */
function satya_class_schedule_shortcode($atts) {
    global $post, $wpdb;

    extract(shortcode_atts(array(
        // 'type' => 'regular',
    ), $atts));

    $weekdays = satya_weekdays();
    $options = satya_weekday_option_names();
    // $moon_options = satya_moondays_option_names();

    // Get moon days
    $new = satya_get_moon('new_moon');
	$full = satya_get_moon('full_moon');


	// Get start of week
	$date = new DateTime();
    $day_of_week = $date->format("w");
    $date->modify("-$day_of_week day");


    //Time format
	$format = 'g:i a';

	$lang = pll_current_language();

	$shedule = get_option('theme-general-settings');

	ob_start();

	if ( is_page_template( 'page-templates/class-schedule.php' ) ) {
		$background = get_the_post_thumbnail_url( $post->ID,  'full' );
	} else {
		$page = get_page_by_title( 'Horarios' );
		$background = get_the_post_thumbnail_url( $page->ID,  'full' );
	}

    

	?>

	<style scoped>
	/*.class-schedule {
	    background-image: url(<?php echo $background; ?>); 
	}*/
	@supports (display: grid) {
		@media (min-width: 1024px) {
		    .class-schedule {
		        background-image: url(<?php echo $background; ?>);
		        background-image: linear-gradient(rgba(0,0,0,.2), rgba(0,0,0,0)), url(<?php echo $background; ?>);
		        background-size: cover;
		    }
		}
	}
	</style>
	<section class="class-schedule loading">

		<?php 

		$column = '';
		// $row = '';

		$track = array();
		
		foreach ($options as $key => $day) {

			$count = get_option( $day );

			$column = substr ( strtolower($weekdays[$key][1]), 0, 3);

			?>

			<h3 class="schedule-weekday" data-col="<?php echo $column; ?>"><?php echo $weekdays[$key][1]; ?></h3>

			<?php 

			// Add 1 day to date
			$date->modify("+1 day");

			if( $date->format('Y-m-d') == $full->format('Y-m-d') ) { ?>
			
				<div class="full-moon-day" data-col="<?php echo $column; ?>">
					<?php _e('Full moon, no classes', 'satya'); ?>
				</div>
			
			<?php } elseif ( $date->format('Y-m-d') == $new->format('Y-m-d') ) { ?>
			
				<div class="new-moon-day" data-col="<?php echo $column; ?>">
					<?php  _e('New moon, no classes', 'satya'); ?>
				</div>
			
			<?php } else {

				if ( $count ) {
					
					$day_classes = array();

					for( $i = 0; $i < $count; $i++ ) {
						
						// Get class id
						$class_id = esc_html( get_option( $day.'_' . $i . '_class' ) );

						// Get language of class
						$post_lang = pll_get_post_language($class_id);

						// If class language doesn't match current language and a translation exists
						if ( $lang != $post_lang && pll_get_post($class_id) ) {
							$class_id = pll_get_post($class_id);
						}



						$teacher_id = esc_html( get_option( $day.'_' . $i . '_teacher' ) );
						$start = esc_html( date( $format, strtotime( get_option( $day.'_' . $i . '_time_start' ) ) ) );
						$end = esc_html( date( $format, strtotime( get_option( $day.'_' . $i . '_time_end' ) ) ) );
						$pay_separate = esc_html( get_option( $day.'_' . $i . '_pay_separate' ) );
						$cancelled = esc_html( get_option( $day.'_' . $i . '_cancelled' ) );
						$types = get_option( $day.'_' . $i . '_class_type' );

						// echo '<pre>';
						// echo $start.' - '.$end;
						// var_dump($types);
						// echo '</pre>';


						foreach ( $types as $key => $value) {
							if ($value == 'inperson') {
								$types[$key] = __( 'In person', 'satya' );
							}
						}

						// echo '<pre>';
						// echo $start.' - '.$end;
						// var_dump($types);
						// echo '</pre>';

						$day_classes[$start][] = array(
							'class_id'		=> $class_id,
							'teacher_id'	=> $teacher_id,
							'end'			=> $end,
							'pay_separate'	=> $pay_separate,
							'cancelled'		=> $cancelled,
							'type'			=> $types
						);

					}

					foreach ($day_classes as $key => $classes) {

						$start = $key;
						$count_classes = count($classes);
						
						foreach ($classes as $key => $class) {

							$css_class = 'class';

							if ( $class['cancelled'] ) {
								$css_class .= ' cancelled';
							} 

							if ( $class['pay_separate'] ) {
								$css_class .= ' pay-separate';
							}

							if ( $count_classes > 1 ) {
								if ( $key == 0 ) {
									$css_class .= ' same-time-back';
								} elseif ( $key == 1 ) {
									$css_class .= ' same-time-front';
								}
							}

							if ( is_array($class['type']) && !empty($class['type']) ) {
								$types = $class['type'];
							}
							?>

							<div class="<?php echo $css_class; ?>" data-start="<?php echo $start; ?>" data-end="<?php echo $class['end']; ?>" data-col="<?php echo $column; ?>">
								<span class="class-container">
									<h4 class="class-name"><?php echo get_the_title( $class['class_id'] ); ?> <?php  ?></h4>
									
									<span class="class-time"><?php echo $start; ?> - <?php echo $class['end']; ?></span>

									<span class="class-teacher" data-link="<?php echo get_permalink( $class['teacher_id'] ); ?>"><?php echo get_the_title( $class['teacher_id'] ); ?></span>
									
									
									<?php if ( $class['cancelled'] ) { ?>
										<span class="class-notice"><?php _e('Cancelled', 'satya'); ?></span>
									<?php }

									if ( !empty($types) ) {
										echo '<ul class="class-types">';
										foreach ( $types as $value) {
											echo '<li>';
											echo esc_html__( $value, 'satya' );
											echo '</li>';
										}
										echo '</ul>';
									}
									?>
								</span>
							</div>							

						<?php }
					}
				}
			}
		}

		?>

	</section>

	<?php
    $output = ob_get_clean();

    return $output;

}