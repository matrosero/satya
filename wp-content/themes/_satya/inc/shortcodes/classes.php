<?php
/*
 * Class schedule callback
 * - [list-classes]
 *
 * Returns list of team members
 */
function satya_list_classes_shortcode($atts) {
    global $post, $wpdb;

    extract(shortcode_atts(array(
        // 'type' => 'regular',
    ), $atts));


    $output = '';

   	/*
	 * The WordPress Query class.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/WP_Query
	 */
	$args = array(


		// Type & Status Parameters
		'post_type'   => 'class',
		'post_status' => 'publish',

		// Order & Orderby Parameters
		'order'               => 'DESC',
		'orderby'             => 'date',

		// Pagination Parameters
		'posts_per_page'         => -1,

		// Parameters relating to caching
		'no_found_rows'          => false,
		'cache_results'          => true,
		'update_post_term_cache' => true,
		'update_post_meta_cache' => true,

	);
    
    $query = new WP_Query( $args );

    if ($query->have_posts()) :

    	ob_start(); ?>

    	<section class="our-classes" id="regular-classes">

    		<h2 class="section-title"><?php _e('Regular classes', 'satya'); ?></h2>

    		<div class="classes-list">

		    	<?php
		    	$do_not_duplicate = array();
		        while ($query->have_posts()) : $query->the_post();

		        	if ( has_term( 'regulares', 'class_type' ) ) {
			            $do_not_duplicate[] = $post->ID;

			        	if ( get_the_content() ) {
			        		get_template_part( 'template-parts/content/class','loop' );
			        	}
			        }
		            
		        endwhile; 
		        $query->rewind_posts();
		        ?>
		    </div>

        </section>

        <section class="our-classes" id="special-classes">

    		<h2 class="section-title"><?php _e('Special classes', 'satya'); ?></h2>

    		<div class="classes-list">

		    	<?php
		        while ($query->have_posts()) : $query->the_post();

		        	if ( in_array( $post->ID, $do_not_duplicate ) ) continue;

		        	if ( get_the_content() ) {
		        		get_template_part( 'template-parts/content/class','loop' );
		        	}
		            
		        endwhile; ?>

		    </div>

        </section>

    <?php endif;
    wp_reset_postdata();
    
	$output = ob_get_clean();
 	
    return $output;

}