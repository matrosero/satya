<?php
/*
 * Class schedule callback
 * - [list-classes]
 *
 * Returns list of team members
 */
function satya_toc_shortcode($atts, $content = null) {
    global $post, $wpdb;

    extract(shortcode_atts(array(
        // 'type' => 'regular',
    ), $atts));



   	// Array to hold TOC elements
	$toc = array();

	$the_content = get_the_content();
	
	$i = 0;

	// Regular expression to match headings    	
	$regex = '/(\<h[1-6](.*?))\>(.*)(<\/h[1-6]>)/i';

	// Match all headings
	preg_match_all($regex, $the_content, $matches);

	// Populate array with headings
	foreach ($matches[3] as $key => $value) {
		$toc[] = array(
			$value,
			satya_string_to_link($value)
		); 
	}

	$the_content = preg_replace_callback( $regex, function( $matches ) {
		if ( ! stripos( $matches[0], 'id=' ) ) :
			$matches[0] = $matches[1] . $matches[2] . ' id="' . sanitize_title( $matches[3] ) . '">' . $matches[3] . $matches[4];
		endif;
		return $matches[0];
	}, $the_content );

	$return = satya_build_toc($toc, $content);

    // ob_start(); 


    
	// $output = ob_get_clean();
 	
    return $return;

}