<?php

/**
 * Register all shortcodes
 *
 * @return null
 */
function satya_register_shortcodes() {
    add_shortcode('class-schedule', 'satya_class_schedule_shortcode');
    add_shortcode('list-classes', 'satya_list_classes_shortcode');
    add_shortcode('moon-next', 'satya_moon_next_shortcode');
    add_shortcode('indice-contenidos', 'satya_toc_shortcode');
}
add_action( 'init', 'satya_register_shortcodes' );


/* Schedule. */
require_once get_template_directory() . '/inc/shortcodes/schedule.php';

/* Classes. */
require_once get_template_directory() . '/inc/shortcodes/classes.php';

/* Moon. */
require_once get_template_directory() . '/inc/shortcodes/moon.php';

/* ToC. */
require_once get_template_directory() . '/inc/shortcodes/toc.php';