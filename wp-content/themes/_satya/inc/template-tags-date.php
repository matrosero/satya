<?php
/**
 * Custom date template tags for this theme
 *
 * @package satya
 */


/*
 * Is future date
 */
if ( ! function_exists( 'satya_is_future' ) ) :
	function satya_is_future( $post_id = null, $format = 'Y-m-d' ) {
		global $post;

		if ( !$post_id ) {
			$post_id = $post->ID;
		}
		
		$today = date('Y-m-d');
		$event_date = get_the_date($format,$post->ID);


		if ( get_field('event_date_type') == 'single-date' ) {
			if ($event_date >= $today) {
				return true;
			}
		} elseif ( get_field('event_date_type') == 'date-range' ) {
			
			if ( $today <= get_field('event_date_end') ) {
				return true;
			}

		} elseif ( get_field('event_date_type') == 'several-dates' ) {
			if ($event_date >= $today) {
				return true;
			} elseif( have_rows('additional_dates') ) {

				// Loop through rows.
				while( have_rows('additional_dates') ) : the_row();

					// Load sub field value.
					if ( get_sub_field('date') >= $today ) {
						return true; 
					}
			
				// End loop.
				endwhile;

			}
		}
	}
endif;

/*
 * Proper capitalization for spanish dates
 */
if ( ! function_exists( 'satya_capitalize_spanish_date' ) ) :
	function satya_capitalize_spanish_date( $date = null, $format = 'j \d\e F' ) {
		
		if ( !$date ) {
			$date = date($format);
		} 

		return ucfirst( strtolower( $date ) );
	}
endif;

/*
 * Simple date from a timestamp
 */
if ( ! function_exists( 'satya_get_moon' ) ) :
	function satya_get_moon( $phase = null ) {
		
		if ( $phase != 'full_moon' && $phase != 'new_moon' ) {
			return false;
		} 

		// Get moons
		if ( !isset($moon) ) {
			$moon = new Solaris\MoonPhase();
		} 

		$date = new DateTime(gmdate('Y-m-d H:i:s', $moon->get_phase($phase)), new DateTimeZone('UTC'));
		$date->setTimeZone(new DateTimeZone('America/Costa_Rica'));




		// var_dump($date);
		return $date;
	}
endif;


/*
 * Spanish date from a timestamp
 */
if ( ! function_exists( 'satya_timestamp_to_spanish_date' ) ) :
	function satya_timestamp_to_spanish_date( $timestamp = null, $format = 'j \d\e F' ) {
		
		// echo '<pre>';var_dump($timestamp);echo '</pre>';

		if ( $timestamp ) {
			return strtolower ( date_i18n ( $format, $timestamp ) );
		}
	}
endif;


/*
 * Convert date to WP DB date format
 */
if ( ! function_exists( 'satya_wp_date' ) ) :
	function satya_wp_date( $date = null ) {
		if ( $date) {
			$wp_date = date("Y-m-d", strtotime($date));
			return $wp_date;
		}
	}
endif;


/*
 * Return formated and readable markup for event with date range
 */
if ( ! function_exists( 'satya_readable_event_daterange' ) ) :
	function satya_readable_event_daterange( $date = null ) {
		if ( $date ) {

			$return = '<p>';

			if( pll_current_language() == 'en' ) {
				$format = 'F j';

				$return .= $date['start']->format($format).' – ';

				// Same month
				if ( $date['start']->format('n') == $date['end']->format('n') ) {
					$format = 'j';
				} 

				$return .= $date['end']->format($format);

				

			} else {
				$format = 'j \d\e F';


				// Same month
				if ( $date['start']->format('n') == $date['end']->format('n') ) {
					$return .= $date['start']->format('j');
				} else {
					$return .= strtolower ( date_i18n ($format, $date['start']->getTimestamp() ) );
				}

				$return .= ' – '. strtolower ( date_i18n ($format, $date['end']->getTimestamp() ) );

			}


			$return .= ', '.$date['end']->format('Y');

			$return = '<p>'.$return.'</p>';

			return $return;
		}
	}
endif;


/*
 * Returns dates array for event with date range
 */
if ( ! function_exists( 'satya_event_date_range' ) ) :
	function satya_event_date_range() {
		
		global $post;

		// Get first date
		$dates = array(
				'start'	=> new DateTime( get_the_date( 'Y-m-d' ).' '.get_post_meta( $post->ID, 'event_time', 1 ) ),
				'end'	=> new DateTime( satya_wp_date( get_post_meta( $post->ID, 'event_date_end', 1 ) ).' '.get_post_meta( $post->ID, 'event_time_end', 1 ) ),
		);

		return $dates;		
	}
endif;


/*
 * Return formated and readable markup for event with several dates
 */
if ( ! function_exists( 'satya_readable_event_dates' ) ) :
	function satya_readable_event_dates( $date = null ) {
		if ( $date ) {

			$timestamp = strtotime($date['start']->format('Y-m-d'));

			$return = '<time datetime="' . $date['start']->format('Y-m-d H:i') . '">'
				.satya_timestamp_to_spanish_date( $timestamp )
				. '<br />'
				// .'<br />De '
				.$date['start']->format('g:i a')				
				.'</time>'
				.' a '
				.'<time datetime="' . $date['end']->format('Y-m-d H:i') . '">'
				.$date['end']->format('g:i a')
				.'</time>';

			return $return;
		}
	}
endif;


/*
 * Returns dates array for event with several dates
 */
if ( ! function_exists( 'satya_event_several_dates' ) ) :
	function satya_event_several_dates( $return_format = 'j \d\e F' ) {
		
		global $post;

		$grab_format = 'Y-m-d';

		// Get first date
		$dates = array(
			array(
				'start'	=> new DateTime( get_the_date( 'Y-m-d' ).' '.get_post_meta( $post->ID, 'event_time', 1 ) ),
				'end'	=> new DateTime( get_the_date( 'Y-m-d' ).' '.get_post_meta( $post->ID, 'event_time_end', 1 ) ),
			),
		);


		$count = get_post_meta( $post->ID, 'additional_dates', true );

		for( $i = 0; $i < $count; $i++ ) {
			
			$date = satya_wp_date( get_post_meta( get_the_ID(), 'additional_dates_' . $i . '_date', true ) );

			$start = get_post_meta( get_the_ID(), 'additional_dates_' . $i . '_time_start', true );

			$end = get_post_meta( get_the_ID(), 'additional_dates_' . $i . '_time_end', true );

			$dates[] = array(
				'start'	=> new DateTime( $date . ' ' . $start ),
				'end'	=> new DateTime( $date . ' ' . $end ),
			);

		}

		return $dates;

	}
endif;


/*
 * Returns date for event with single date
 */
if ( ! function_exists( 'satya_event_single_date' ) ) :
	function satya_event_single_date() {
		
		global $post;

		$grab_format = 'Y-m-d';

		// Get first date
		$date = array(
			'start'	=> new DateTime( get_the_date( 'Y-m-d' ).' '.get_post_meta( $post->ID, 'event_time', 1 ) ),
			'end'	=> new DateTime( get_the_date( 'Y-m-d' ).' '.get_post_meta( $post->ID, 'event_time_end', 1 ) ),
		);

		return $date;

	}
endif;


/*
 * Returns array withevent time/schedule
 */
if ( ! function_exists( 'satya_event_readable_time' ) ) :
	function satya_event_readable_time($date = null) {

		if ( $date ) {

			$return = '<time datetime="' . $date['start']->format('H:i') . '">'
				.$date['start']->format('g:i a')				
				.'</time>'
				.' – '
				.'<time datetime="' . $date['end']->format('H:i') . '">'
				.$date['end']->format('g:i a')
				.'</time>';

			return $return;
		}

		return $time;

	}
endif;


/*
 * Format date for column style (home, list of events)
 *
 */
if ( ! function_exists( 'satya_format_date_column' ) ) :
	function satya_format_date_column( $date = null, $show_weekday = false, $show_year = false ) {
		if ( !$date ) {
			return;
		}

		// l = Sunday | D = Sun
		if ( $show_weekday ) {
			$weekday = date_i18n( 'D', strtotime( $date ) );
		}

		if ( $show_year ) {
			$year = date_i18n( 'Y', strtotime( $date ) );
		}
		
		// d = Mon
		$day = date_i18n( 'j', strtotime( $date ) );
		$month = date_i18n( 'M', strtotime( $date ) );

		$return = '<div class="date">';

		if ( $show_weekday ) {
			$return .= '<span class="day-week">'.$weekday.'</span>';
		}
		
		$return .= '<span class="day">'.$day.'</span>
				<span class="month">'.$month.'</span>';
		
		if ( $show_year ) {
			$return .= '<span class="year">'.$year.'</span>';
		}

		$return .= '</div>';

		return $return;
	}
endif;

/*
 * Get column-style date (home, list of events)
 *
 */
if ( ! function_exists( 'satya_event_date_column' ) ) :
	function satya_event_date_column( $id = null, $show_weekday = false ) {
		global $post; 
		if ( !$id ) {
			$id = $post->ID;
		}

		// $date = get_post_meta( $id, 'event_date', true );
		$date = get_the_date('Ymd', $id);


		if ( $show_weekday ) {
			
			$date = satya_format_date_column( $date, true );


		} elseif ( get_post_type( $id ) == 'post' ) {
			
			$date = satya_format_date_column( $date, false, true );

			// satya_format_date_column( $date = null, $show_weekday = false, $show_year = false )

		} else {
			
			$date = satya_format_date_column( $date, false, true );
		}

		return $date;
	}
endif;