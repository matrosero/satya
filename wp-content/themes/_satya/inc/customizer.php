<?php
/**
 * Satya 2019 Theme Customizer
 *
 * @package satya
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function satya_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'satya_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'satya_customize_partial_blogdescription',
		) );
	}


	//Social Media
	$wp_customize->add_section( 'satya_socialmedia', array(
			'title'    => __('Social Media Accounts', 'satya'),
			'priority' => 35,
	) );


	//Next CLass Notice
	$wp_customize->add_section( 'satya_settings', array(
		'title'    => __('Next Class Notice', 'satya'),
		'priority' => 30,
	) );

	$wp_customize->add_setting( 'satya_next_class', array(
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'esc_url_raw'
	) );

	$wp_customize->add_control( 'satya_next_class', array(
		'label'    => __( 'Show next class notice', 'satya' ),
		'description' => __( 'Marcar la caja para que se muestre la notificación sobre la próxima clase.', 'satya' ),
		'section'  => 'satya_settings',
		'type'     => 'checkbox',
		'priority' => 1,
	) );




	$social_sites = satya_social_media_array();
	$priority = 5;

	foreach($social_sites as $social_site) {

		$social_site_name = ucfirst($social_site);

		$wp_customize->add_setting( "$social_site", array(
				'type'              => 'theme_mod',
				'capability'        => 'edit_theme_options',
				'sanitize_callback' => 'esc_url_raw'
		) );

		$wp_customize->add_control( $social_site, array(
				'label'    => __( "$social_site_name url:", 'satya' ),
				'section'  => 'satya_socialmedia',
				'type'     => 'text',
				'priority' => $priority,
		) );

		$priority = $priority + 5;
	}
}
add_action( 'customize_register', 'satya_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function satya_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function satya_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function satya_customize_preview_js() {
	wp_enqueue_script( 'satya-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'satya_customize_preview_js' );
