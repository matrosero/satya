<?php

/**
 * Enqueue scripts and styles.
 */
function satya_scripts() {
    global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    // Modernizr in head
    wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/scripts/modernizr-custom.js', array( 'jquery' ), '3.6.0', false );


    // Adding scripts file in the footer
    wp_enqueue_script( 'satya-js', get_template_directory_uri() . '/assets/scripts/scripts.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts'), true );

    // Register main stylesheet
	wp_enqueue_style( 'satya-style', get_template_directory_uri() . '/assets/styles/style.css', array(), filemtime(get_template_directory() . '/sass'), 'all' );

	wp_enqueue_script( 'satya-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

    wp_enqueue_script( 'satya-panel', get_template_directory_uri() . '/assets/scripts/panel.js', array( 'jquery','modernizr' ), filemtime(get_template_directory() . '/assets/scripts'), true );

	wp_enqueue_script( 'satya-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

    if ( is_page_template( 'page-templates/contact.php' ) ) {
        write_log('is contact');
        // wp_enqueue_script( 'google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAQPq7wGURbKFVOG5KGLiA8wH7i0A1cGHY', array('') );
        
        // wp_enqueue_script( 'google-maps', plugin_dir_url( __FILE__ ) . 'includes/js/google-maps.js', array('jquery','google-maps-api') );
    } 

    /*
     * Google Fonts
     *
     * font-family: 'Fira Sans', sans-serif;
     * font-family: 'Yeseva One', cursive;
     */
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Fira+Sans:400,400i,700,700i|Yeseva+One', false );
}
add_action( 'wp_enqueue_scripts', 'satya_scripts' );


/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Seventeen 1.0
 */
function satya_javascript_detection() {
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'satya_javascript_detection', 0 );



/**
 * Preload scripts in footer.
 *
 * https://macarthur.me/posts/preloading-javascript-in-wordpress
 *
 * @since Twenty Seventeen 1.0
 */
add_action('wp_head', function () {

    global $wp_scripts;

    foreach($wp_scripts->queue as $handle) {
        $script = $wp_scripts->registered[$handle];

        //-- Weird way to check if script is being enqueued in the footer.
        if( isset($script->extra['group']) && $script->extra['group'] === 1) {

            //-- If version is set, append to end of source.
            $source = $script->src . ($script->ver ? "?ver={$script->ver}" : "");

            //-- Spit out the tag.
            echo "<link rel='preload' href='{$source}' as='script'/>\n";
        }
    }
}, 1);