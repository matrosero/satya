<?php
/**
 * Register menus
 */
register_nav_menus(
	array(
		'main-nav'		=> __( 'The Main Menu', 'jointswp' ),
        'tablet-nav'      => __( 'The Tablet Menu', 'jointswp' ),
        'mobile-nav'    => __( 'The Mobile Menu', 'jointswp' ), 
		'offcanvas-nav'	=> __( 'The Off-Canvas Menu', 'jointswp' ),	
		'footer-links'	=> __( 'Footer Links', 'jointswp' )	
	)
);

/**
 * Limit main menu to 3 items
 * SRC: https://www.isitwp.com/limit-amount-of-menu-items/
 */
// add_filter( 'wp_nav_menu_objects', 'satya_limit_menu_items', 10, 2 );
function satya_limit_menu_items($items, $args) {
    // want our MAINMENU to have MAX of 7 items
    if ( $args->theme_location == 'main-nav' || $args->theme_location == 'mobile-nav' ) {
        $toplinks = 0;
        foreach ( $items as $k => $v ) {
            if ( $v->menu_item_parent == 0 ) {
                // count how many top-level links we have so far...
                $toplinks++;
            }
            // if we've passed our max # ...
            if ( $toplinks > 3 ) {
                unset($items[$k]);
            }
        }
    }
    return $items;
}

add_filter( 'wp_nav_menu_items', 'satya_add_menu_toggle_to_nav', 10, 2 );
function satya_add_menu_toggle_to_nav( $items, $args ) {
    

    /*
    if( $args->theme_location == 'main-nav' || $args->theme_location == 'mobile-nav' ){

        if( function_exists('pll_the_languages') && function_exists('pll_get_post_translations') && count( pll_get_post_translations( get_the_ID() ) ) > 1) {

            $items .= '<li class="menu-item language-switcher-navigation"><ul>'.pll_the_languages(array('display_names_as'=>'slug','echo'=>0)).'</ul></li>';

        }
    }
    */

    if( $args->theme_location == 'mobile-nav'  || $args->theme_location == 'tablet-nav' ) {

        $items .= '<li class="menu-item"><button class="menu-more-toggle" id="toggle-menu" href="#">'.__('Menu', 'satya').'</button></li>';

        // $translations = pll_the_languages(
        //     array(
        //         'raw' => 1,
        //         'hide_current' => 1,
        //         'display_names_as' => 'slug'
        //     ));

        // foreach ($translations as $key => $translation) {
        //     $items .=  '<li class="'.implode(' ',$translation['classes']).'"><a hreflang="'.$translation['locale'].'" href="'.$translation['url'].'" lang="'.$translation['locale'].'">'.strtoupper($translation['name']).'</a></li>';
        // }
    }
        
    if( $args->theme_location == 'offcanvas-nav' ){
        $items = '<li id="" class="panel-logo menu-item">
                <a href="'.home_url().'" rel="home" itemprop="url"><img src="'.get_stylesheet_directory_uri().'/assets/images/logo-satya-horizontal.svg" alt="Ashtanga Yoga Satya" /></a>
            </li>' . $items;
    }
    return $items;
}