<?php
if (!class_exists('Satya_Events_Grid')) :
    /**
     * Adds Satya_Events_Grid widget.
     */
    class Satya_Events_Grid extends Satya_Widget_Base
    {
        /**
         * Sets up a new widget instance.
         *
         * @since 1.0.0
         */
        function __construct()
        {
            $this->text_fields = array('satya-events-grid-title');

            $this->boolean = array('satya-title-in-grid');

            $this->number_fields = array('satya-posts-per-page');


            $widget_ops = array(
                'classname' => 'events-grid',
                'description' => __('Displays events in a grid.', 'satya'),
                'customize_selective_refresh' => true,
            );

            parent::__construct('satya_events_grid', __('Satya Events Grid', 'satya'), $widget_ops);
        }

        /**
         * Front-end display of widget.
         *
         * @see WP_Widget::widget()
         *
         * @param array $args Widget arguments.
         * @param array $instance Saved values from database.
         */

        public function widget($args, $instance)
        {

            $instance = parent::satya_sanitize_data($instance, $instance);


            /** This filter is documented in wp-includes/default-widgets.php */

            $title = __('Events &amp; workshops', 'satya');

            $number_of_posts = isset($instance['satya-posts-per-page']) ? intval($instance['satya-posts-per-page']) : '1';
            

            $posts = satya_get_events($number_of_posts); 

            $link = get_post_type_archive_link( 'event' );

            $count = $posts->post_count;

            $exclude = array();

            // open the widget container
            echo $args['before_widget'];

            satya_show_block_header($title, $link, 'events');

            if ($posts->have_posts()) :

                while ($posts->have_posts()) : $posts->the_post();
                    

                    $exclude[] = get_the_ID();

                    get_template_part( 'template-parts/content/event', 'loop' );
                endwhile;
                             
            endif;



            if ( $number_of_posts > $count) {
                $new_number_of_posts = $number_of_posts - $count;

                $other_posts = satya_get_events($new_number_of_posts, false, $exclude, true );

                if ($other_posts->have_posts()) :

                    while ($other_posts->have_posts()) : $other_posts->the_post();
                        
                        get_template_part( 'template-parts/content/event', 'loop' );
                    endwhile;
                                 
                endif;

            }

            // close the widget container
            echo $args['after_widget'];

            wp_reset_postdata();



        }

        /**
         * Back-end widget form.
         *
         * @see WP_Widget::form()
         *
         * @param array $instance Previously saved values from database.
         */
        public function form($instance)
        {
            $this->form_instance = $instance;
            // $options = array(
            //     'full-plus-list' => __('Big thumb in first and other in list', 'satya'),
            //     'list' => __('All in list', 'satya')

            // );


            //print_pre($terms);

            // echo parent::satya_generate_text_input('satya-events-grid-title', __('Title', 'satya'), 'Eventos');
            echo parent::satya_generate_boolean_input('satya-title-in-grid', __('Title as part of grid', 'satya'), 'Eventos');
            echo parent::satya_generate_text_input('satya-posts-per-page', __('Number of events to show', 'satya'), 0, 'number');


            //print_pre($terms);


        }

    }
endif;