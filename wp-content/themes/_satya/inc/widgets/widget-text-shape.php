<?php
if (!class_exists('Satya_Text_Shape')) :
    /**
     * Adds Satya_Text_Shape widget.
     */

	class Satya_Text_Shape extends WP_Widget {

        /**
         * Sets up a new widget instance.
         *
         * @since 1.0.0
         */
        function __construct()
        {
            // $this->text_fields = array('satya-events-grid-title');

            // $this->boolean = array('satya-title-in-grid');

            // $this->number_fields = array('satya-posts-per-page');


            $widget_ops = array(
                'classname' => 'feat-text-shape',
                'description' => __('Featured, larger text with decorative shape.', 'satya'),
                'customize_selective_refresh' => true,
            );

            parent::__construct('satya_text_shape', __('Satya Text with Shape', 'satya'), $widget_ops);
        }

	    // function Satya_Text_Shape() {

	    //     $widget_ops = array(
        //         'classname' => 'feat-text-shape',
        //         'description' => __('Featured, larger text with decorative shape.', 'satya'),
        //         'customize_selective_refresh' => true,
        //     );


	    //     $this->WP_Widget('satya_text_shape', __('Satya Text with Shape'), $widget_ops);
	    // }


		// The widget form (for the backend )
		function form( $instance ) {

			// Set widget defaults
			$defaults = array(
				'title'    => '',
				'textarea' => '',
				'select'   => '',
			);
			
			// Parse current settings with defaults
			extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

			<?php // Widget Title ?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Widget Title', 'satya' ); ?></label>
				
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>

			<?php // Textarea Field ?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'textarea' ) ); ?>"><?php _e( 'Textarea:', 'satya' ); ?></label>
				
				<textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'textarea' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'textarea' ) ); ?>"><?php echo wp_kses_post( $textarea ); ?></textarea>
			</p>

			<?php // Dropdown ?>
			<p>
				<label for="<?php echo $this->get_field_id( 'select' ); ?>"><?php _e( 'Select', 'satya' ); ?></label>
				<select name="<?php echo $this->get_field_name( 'select' ); ?>" id="<?php echo $this->get_field_id( 'select' ); ?>" class="widefat">
				<?php
				// Your options array
				$options = array(
					''        => __( 'Select', 'satya' ),
					0 => __( 'No shape', 'satya' ),
					'flower' => __( 'Flower', 'satya' ),
					'circles' => __( 'Circles', 'satya' ),
				);

				// Loop through options and add each one to the select dropdown
				foreach ( $options as $key => $name ) {
					echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $select, $key, false ) . '>'. $name . '</option>';

				} ?>
				</select>
			</p>

		<?php }


		// Update widget settings
		function update( $new_instance, $old_instance ) {
			$instance = $old_instance;
			$instance['title']    = isset( $new_instance['title'] ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
			$instance['textarea'] = isset( $new_instance['textarea'] ) ? wp_kses_post( $new_instance['textarea'] ) : '';
			$instance['select']   = isset( $new_instance['select'] ) ? wp_strip_all_tags( $new_instance['select'] ) : '';
			return $instance;
		}


		// Display the widget
		public function widget( $args, $instance ) {

			extract( $args );

			// Check the widget options
			$title    = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
			$textarea = isset( $instance['textarea'] ) ?$instance['textarea'] : '';
			$select   = isset( $instance['select'] ) ? $instance['select'] : '';

			// WordPress core before_widget hook (always include )
			echo $before_widget;

		   // Display the widget
		   echo '<div class="featured-text bg-'.$select.'">';

				// Display widget title if defined
				// if ( $title ) {
				// 	echo $before_title . $title . $after_title;
				// }

		   		/*
		   		?>
		   		<img class="bg-<?php echo $select; ?>" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/shape-flower-edge.svg" alt="Ashtanga Yoga Satya" />
		   		</php
		   		*/


				// Display textarea field
				if ( $textarea ) {
					echo wpautop($textarea);
				}


			echo '</div>';

			// WordPress core after_widget hook (always include )
			echo $after_widget;

		}




	}







endif;