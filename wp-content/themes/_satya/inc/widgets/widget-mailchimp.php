<?php
if (!class_exists('Satya_Mailchimp_Shortcode')) :
    /**
     * Adds Satya_Mailchimp_Shortcode widget.
     */
    class Satya_Mailchimp_Shortcode extends Satya_Widget_Base
    {
        /**
         * Sets up a new widget instance.
         *
         * @since 1.0.0
         */
        function __construct()
        {
            $this->text_fields = array(
            	'satya-mailchimp-shortcode-title',
            	'satya-mailchimp-text',
            	'satya-mailchimp-shortcode'
            );


            $widget_ops = array(
                'classname' => 'mailchimp-optin',
                'description' => __('Displays Mailchimp opt in form using shortcode.', 'satya'),
                'customize_selective_refresh' => true,
            );

            parent::__construct('satya_mailchimp_shortcode', __('Satya Mailchimp Shortcode', 'satya'), $widget_ops);
        }

        /**
         * Front-end display of widget.
         *
         * @see WP_Widget::widget()
         *
         * @param array $args Widget arguments.
         * @param array $instance Saved values from database.
         */

        public function widget($args, $instance)
        {

            $instance = parent::satya_sanitize_data($instance, $instance);


            /** This filter is documented in wp-includes/default-widgets.php */

            $title = apply_filters('widget_title', $instance['satya-mailchimp-shortcode-title'], $instance, $this->id_base);

            $text = isset($instance['satya-mailchimp-text']) ? ($instance['satya-mailchimp-text']) : '';

            $shortcode = isset($instance['satya-mailchimp-shortcode']) ? ($instance['satya-mailchimp-shortcode']) : '';

            // open the widget container
            echo $args['before_widget'];

            if (!empty($text)) {
            	echo '<p class="call-to-action">'.esc_html($text).'</p>';
            }

            if (!empty($shortcode)) {
            	echo do_shortcode( $shortcode );
            }



            // close the widget container
            echo $args['after_widget'];
        }

        /**
         * Back-end widget form.
         *
         * @see WP_Widget::form()
         *
         * @param array $instance Previously saved values from database.
         */
        public function form($instance)
        {
            $this->form_instance = $instance;
            // $options = array(
            //     'full-plus-list' => __('Big thumb in first and other in list', 'satya'),
            //     'list' => __('All in list', 'satya')

            // );


            //print_pre($terms);

            echo parent::satya_generate_text_input('satya-mailchimp-shortcode-title', __('Title', 'satya'), 'Terapias');

            echo parent::satya_generate_text_input('satya-mailchimp-text', __('Text', 'satya'), 'Quiero recibir noticias de Ashtanga Yoga Satya ');

            echo parent::satya_generate_text_input('satya-mailchimp-shortcode', __('Shortcode', 'satya'), '[mc4wp_form id="827"]');



            //print_pre($terms);


        }

    }
endif;