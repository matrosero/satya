<?php

/**
 * Returns posts.
 *
 * @since CoverNews 1.0.0
 */
if(!function_exists('satya_get_posts')):
    function satya_get_posts($number_of_posts, $post_type = 'post', $tax_id = '0', $taxonomy = null, $excluded = null ) {


        if(is_front_page()) {
            $paged = (get_query_var('page')) ? get_query_var('page') : 1;
        }else {
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        }

        $ins_args = array(
            'post_type' => $post_type,
            'posts_per_page' => absint($number_of_posts),
            'paged' => $paged,
            'post_status' => 'publish',
            'orderby' => 'date',
            'order' => 'DESC'
        );

        if ( $post_type == 'therapy' ) {

            $ins_args['tax_query'] = array(
                array(
                    'taxonomy' => 'satya_therapist',
                    'field'    => 'slug',
                    'terms'    => 'satya',
                ),
            );
        }

        if ( is_array($excluded) ) {
            $ins_args['post__not_in'] = $excluded;
        }

        if ( is_numeric($tax_id) && absint($tax_id) > 0 ) {

            if ( $taxonomy == null ) {
                $ins_args['cat'] = absint($tax_id);
                // var_dump($ins_args);
            } else {
                // var_dump($taxonomy);
                $ins_args['tax_query'] = array(
                    array(
                        'taxonomy' => $taxonomy,
                        'field'    => 'term_id',
                        'terms'    => absint($tax_id),
                    ),
                );
            }

        } elseif ( !is_numeric($tax_id) && $taxonomy != null ) {

            $ins_args['tax_query'] = array(
                array(
                    'taxonomy' => $taxonomy,
                    'field'    => 'slug',
                    'terms'    => $tax_id,
                ),
            );
        }

        $all_posts = new WP_Query($ins_args);
        return $all_posts;
    }

endif;


/**
 * Returns events.
 *
 * @since CoverNews 1.0.0
 */
if(!function_exists('satya_get_events_by_month')):
    function satya_get_events_by_month($month = null, $year = null) {

        if ( $month == null ) {
            $month = gmdate( 'm' , current_time( 'timestamp' ) );
        }

        if ( $year == null ) {
            $year = gmdate( 'Y' , current_time( 'timestamp' ) );
        }

        $start_date = date($year.'-'.$month.'-'.'01'); // First day of the month
        $end_date = date($year.'-'.$month.'-'.'t'); // 't' gets the last day of the month

        // echo $end_date;

        $meta_query = array(
            'key'       => 'bd_event_date',
            'value'     => array($start_date, $end_date),
            'compare'   => 'BETWEEN',
            'type'      => 'DATE'
        );

        $ins_args = array(
            'post_type' => 'bd_event',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'orderby' => 'meta_value',
            'meta_key' => 'bd_event_date',
            'order' => 'ASC',
            'meta_query' => array(
                $meta_query
            ),
        );


        $all_posts = new WP_Query($ins_args);
        return $all_posts;
    }

endif;


/**
 * Returns all categories.
 *
 * @since CoverNews 1.0.0
 */
if (!function_exists('satya_get_terms')):
function satya_get_terms( $taxonomy='category', $category_id = 0, $default='' ){

    $taxonomy = !empty($taxonomy) ? $taxonomy : 'category';

    if ( $category_id > 0 ) {
            $term = get_term_by('id', absint($category_id), $taxonomy );
            if($term)
                return esc_html($term->name);


    } else {
        $terms = get_terms(array(
            'taxonomy' => $taxonomy,
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => true,
        ));

        $label = "Category";

        if (isset($terms) && !empty($terms)) {
            foreach ($terms as $term) {
                if( $default != 'first' ){
                    $array['0'] = __('Select '.$label, 'satya');
                }
                $array[$term->term_id] = esc_html($term->name);
            }

            return $array;
        }
    }
}
endif;


/**
 * Returns all categories.
 *
 * @since CoverNews 1.0.0
 */
// if (!function_exists('covernews_get_terms_link')):
// function covernews_get_terms_link( $category_id = 0 ){

//     if (absint($category_id) > 0) {
//         return get_term_link(absint($category_id), 'category');
//     } else {
//         return get_post_type_archive_link('post');
//     }
// }
// endif;

/**
 * Returns word count of the sentences.
 *
 * @since CoverNews 1.0.0
 */
// if (!function_exists('covernews_get_excerpt')):
//     function covernews_get_excerpt($length = 25, $covernews_content = null, $post_id = 1) {
//         $length          = absint($length);
//         $source_content  = preg_replace('`\[[^\]]*\]`', '', $covernews_content);
//         $trimmed_content = wp_trim_words($source_content, $length, '...');
//         return $trimmed_content;
//     }
// endif;

/**
 * Returns no image url.
 *
 * @since CoverNews 1.0.0
 */
// if(!function_exists('covernews_no_image_url')):
//     function covernews_no_image_url(){
//         $url = get_template_directory_uri().'/assets/images/no-image.png';
//         return $url;
//     }

// endif;
