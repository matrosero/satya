<?php
if (!class_exists('Satya_Therapies_Grid')) :
    /**
     * Adds Satya_Therapies_Grid widget.
     */
    class Satya_Therapies_Grid extends Satya_Widget_Base
    {
        /**
         * Sets up a new widget instance.
         *
         * @since 1.0.0
         */
        function __construct()
        {
            $this->text_fields = array('satya-grid-title');

            $this->boolean = array('satya-title-in-grid');

            $this->number_fields = array('satya-posts-per-page');


            $widget_ops = array(
                'classname' => 'therapies-grid',
                'description' => __('Displays events in a grid.', 'satya'),
                'customize_selective_refresh' => true,
            );

            parent::__construct('satya_therapies_grid', __('Satya Therapies Grid', 'satya'), $widget_ops);
        }

        /**
         * Front-end display of widget.
         *
         * @see WP_Widget::widget()
         *
         * @param array $args Widget arguments.
         * @param array $instance Saved values from database.
         */

        public function widget($args, $instance)
        {

            $instance = parent::satya_sanitize_data($instance, $instance);


            /** This filter is documented in wp-includes/default-widgets.php */

            $title = __('Holistic therapies', 'satya');

            $link = get_permalink( get_page_by_path( 'terapias-holisticas' ) );

            $number_of_posts = isset($instance['satya-posts-per-page']) ? intval($instance['satya-posts-per-page']) : '1';


            // open the widget container
            echo $args['before_widget'];
            
            $all_posts = satya_get_posts($number_of_posts, 'therapy', '0', null);

            if ( $all_posts->post_count > 6) {
                satya_show_block_header($title);
            }

            if ($all_posts->have_posts()) :

                satya_show_block_header($title, $link, __('therapies', 'satya'));

                while ($all_posts->have_posts()) : $all_posts->the_post();
                    get_template_part( 'template-parts/content/therapy', 'loop' );
                endwhile;
                
            endif;
            wp_reset_postdata();


            // close the widget container
            echo $args['after_widget'];
        }

        /**
         * Back-end widget form.
         *
         * @see WP_Widget::form()
         *
         * @param array $instance Previously saved values from database.
         */
        public function form($instance)
        {
            $this->form_instance = $instance;
            // $options = array(
            //     'full-plus-list' => __('Big thumb in first and other in list', 'satya'),
            //     'list' => __('All in list', 'satya')

            // );


            //print_pre($terms);

            // echo parent::satya_generate_text_input('satya-grid-title', __('Title', 'satya'), 'Eventos');
            echo parent::satya_generate_boolean_input('satya-title-in-grid', __('Title as part of grid', 'satya'), 'Eventos');
            echo parent::satya_generate_text_input('satya-posts-per-page', __('Number of events to show', 'satya'), 0, 'number');


            //print_pre($terms);


        }

    }
endif;