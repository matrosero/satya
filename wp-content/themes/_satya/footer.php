<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package satya
 */

?>

	</main><!-- #main -->

	<footer id="colophon" class="site-footer">
		<div class="footer-logo">
			<a href="<?php echo home_url(); ?>" rel="home" itemprop="url"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-satya-horizontal.svg" alt="Ashtanga Yoga Satya" /></a>
		</div>
		<nav class="footer-navigation">
			<?php
			wp_nav_menu( array(
				'theme_location' => 'footer-links',
				'menu_id'        => 'footer-menu',
				'container'		=> '',
			) );
			?>
		</nav>
		<div class="footer-info">
			<p class="source-org copyright">&copy; 2013 - <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. Todos los derechos reservados.</p>
								
			<p><span class="dev-credits">Diseño y desarrollo web: <a href="https://matilderosero.com" rel="nofollow" title="MRo"><i class="icon-mro"></i></a></span></p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php get_template_part( 'template-parts/navigation/panel' ); ?>
	
<?php wp_footer(); ?>

</body>
</html>
