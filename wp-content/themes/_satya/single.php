<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package satya
 */

get_header();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	if ( have_posts() ) : while ( have_posts() ) : the_post();

		get_template_part( 'template-parts/header/blog' );

		get_template_part( 'template-parts/content/blog', 'single' );

		// the_post_navigation();

		// If comments are open or we have at least one comment, load up the comment template.
		if ( is_singular('post') && ( comments_open() || get_comments_number() ) ) :
			comments_template();
		endif;

	endwhile; // End of the loop.
	endif;
	?>

</article><!-- #post-<?php the_ID(); ?> -->

<?php
// get_sidebar();
get_footer();
