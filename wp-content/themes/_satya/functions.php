<?php
/**
 * Satya 2019 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package satya
 */

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

/**
 * Theme support options.
 */
require get_template_directory() . '/inc/setup.php';

/**
 * WP Head and other cleanup functions.
 */
require get_template_directory() . '/inc/cleanup.php';

/**
 * Register scripts and stylesheets
 */
require get_template_directory() . '/inc/enqueue.php';

/**
 * Register custom menus and menu walkers
 */
require get_template_directory() . '/inc/menu.php';

/**
 * Register sidebars/widget areas
 */
require get_template_directory() . '/inc/widgets.php';


/**
 * Load Init for Hook files.
 */
require get_template_directory() . '/inc/hooks/hooks-init.php';


/**
 * Remove emoji support
 */
require get_template_directory() . '/inc/disable-emoji.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom template date tags for this theme.
 */
require get_template_directory() . '/inc/template-tags-date.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Social media (works with customizer).
 */
require get_template_directory() . '/inc/social.php';

/**
 * Shortcodes.
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * Customize the WordPress login menu
 */
// require(get_template_directory().'/inc/login.php');

/**
 * Customize the WordPress admin
 */
// require(get_template_directory().'/inc/admin.php');

/**
 * Image/srcset functions for this theme.
 */
require get_template_directory() . '/inc/images.php';

/**
 * Moon phases.
 */
require get_template_directory() . '/inc/vendor/MoonPhase.php';

/**
 * Language.
 */
require get_template_directory() . '/inc/language.php';