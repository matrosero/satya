<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package satya
 */

get_header();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
	if ( have_posts() ) : while ( have_posts() ) : the_post();


		if ( !has_post_thumbnail( $post->ID ) ) {
			get_template_part( 'template-parts/header/simple' );
		} else {
			get_template_part( 'template-parts/header/hero' );
		}

		get_template_part( 'template-parts/content' );

		// the_post_navigation();

	endwhile; // End of the loop.
	endif;
	?>

</article><!-- #post-<?php the_ID(); ?> -->

<?php
// get_sidebar();
get_footer();
