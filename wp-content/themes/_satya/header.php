<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package satya
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'satya' ); ?></a>

	<header id="masthead" class="site-header">

		<div class="site-branding">

			<a class="site-logo" href="<?php echo home_url(); ?>" rel="home" itemprop="url"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon-satya.svg" alt="Ashtanga Yoga Satya" /></a>


			<div class="screen-reader-text">
				<?php
				if ( is_front_page() && is_home() ) :
					?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php
				else :
					?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
				endif;
				$satya_description = get_bloginfo( 'description', 'display' );
				if ( $satya_description || is_customize_preview() ) :
					?>
					<p class="site-description"><?php echo $satya_description; /* WPCS: xss ok. */ ?></p>
				<?php endif; ?>
			</div>

		</div><!-- .site-branding -->

		<div class="site-navigation-container">

			<nav class="language-switcher-navigation">
				<?php 
				if( function_exists('pll_the_languages') && function_exists('pll_get_post_translations') ) { 

					if ( count( pll_get_post_translations( get_the_ID() ) ) > 1 ) { ?>

					
						<ul class="menu">
							<?php 
							pll_the_languages(array('display_names_as'=>'slug'));
							?>
						</ul>
					

					<?php } ?>

				<?php } ?>

			</nav>

			<nav id="site-navigation" class="main-navigation">
				<?php
				wp_nav_menu( array(
					'theme_location'	=> 'mobile-nav',
					'menu_class'		=> 'menu small-menu',
					'menu_id'        	=> 'main-menu',
					'container'			=> '',
				) );

				wp_nav_menu( array(
					'theme_location' 	=> 'tablet-nav',
					'menu_class'		=> 'menu medium-menu',
					'menu_id'        	=> 'tablet-menu',
					'container'			=> '',
				) );

				wp_nav_menu( array(
					'theme_location' 	=> 'main-nav',
					'menu_class'		=> 'menu large-menu',
					'menu_id'        	=> 'mobile-menu',
					'container'			=> '',
				) );
				?>
			</nav><!-- #site-navigation -->

			<nav class="social-media-navigation">
				<?php satya_social_media_icons(); ?>
			</nav>

		</div><!-- .topnav-container -->

	</header><!-- #masthead -->

	<main id="main" <?php post_class('site-content'); ?> role="main">