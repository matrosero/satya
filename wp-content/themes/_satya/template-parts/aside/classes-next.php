<?php

if ( get_theme_mod('satya_next_class') ) {

	date_default_timezone_set('America/Costa_Rica');

	if ( !isset($moon) ) {
		$moon = new Solaris\MoonPhase();
	}

	$weekdays = satya_weekdays();
	$option_names = satya_weekday_option_names();
	$moon_phase_options = satya_moondays_option_names();

	$today = new DateTime( null, satya_get_blog_timezone() );
	// $today->setTimeZone(new DateTimeZone('UTC'));


	// $new = satya_get_moon('new_moon');
	// $full = satya_get_moon('full_moon');
	$new = new DateTime(gmdate('Y-m-d H:i:s', $moon->get_phase('new_moon')), new DateTimeZone('UTC'));
	$full = new DateTime(gmdate('Y-m-d H:i:s', $moon->get_phase('full_moon')), new DateTimeZone('UTC'));


	if ( $today > $new ) {
		$new = new DateTime(gmdate('Y-m-d H:i:s', $moon->get_phase('next_new_moon')), new DateTimeZone('UTC'));
	}

	if ( $today > $full ) {
		$full = new DateTime(gmdate('Y-m-d H:i:s', $moon->get_phase('next_full_moon')), new DateTimeZone('UTC'));
	}




	if ( $today->format('Y-m-d') == $full->format('Y-m-d') ) {
		$moon_phase = 'full';
		$moon_phase_text = __( 'full moon', 'satya');
	} elseif ( $today->format('Y-m-d') == $new->format('Y-m-d') ) {
		$moon_phase = 'new';
		$moon_phase_text = __( 'new moon', 'satya');
	}


	if ( isset($moon_phase) ) {
		//Today is moon, change to tomorrow

		$today->modify('+1 day');
		$today->setTime(00, 00);
	}

	// Check if it's Sunday, or if all classes are over (according to hard coded time)
	if ( $today->format('w') == 6 && $today->format('G') > '10' ) {
		// echo 'it is saturday after 10 am, move to monday<br />';
		$today->modify('+2 day');
		$today->setTime(00, 00);
		// echo $today->format('Y-m-d H:i:s').'<br />';
	} elseif ( $today->format('w') == 0 ) {
		// echo 'it is SUNDAY, move to monday<br />';
		$today->modify('+1 day');
		$today->setTime(00, 00);
		// echo $today->format('Y-m-d H:i:s').'<br />';	
	} elseif ( $today->format('Gi') > 1930 ) {
		// echo 'it after 17:30, move to tomorrow<br />';
		$today->modify('+1 day');
		$today->setTime(00, 00);
		// echo $today->format('Y-m-d H:i:s').'<br />';	
	}




	// Subtract one from weekday number so it matches the array
	$option_key = $today->format('w') - 1;

	// echo $option_key.'<br/>';

	$classes = array();

	$format = 'g:i a';

	// Loop through 7 times, increasing the option key +1 on each iteration
	for( $x = 0; $x < 7; $x++ ) {
		
		//If not a moonday, check for classes
		if ( !get_option( $moon_phase_options[$option_key] ) ) {
			
			// Get option name from fixed key
			$option = $option_names[$option_key];

			// Get number of classes for this day
			$count = get_option( $option );
			// echo 'On '.$option_key.' there are '.$count.' classes<br />';

			// IF there are classes
			if ( $count ) {
				
				// Loop through classes
				for( $i = 0; $i < $count; $i++ ) {
					$cancelled = esc_html( get_option( $option.'_' . $i . '_cancelled' ) );
					
					// Check if class is cancelled
					if ( !get_option( $option.'_' . $i . '_cancelled' ) ) {
						
						// Get time
						$start = esc_html( date( 'Gi', strtotime( get_option( $option.'_' . $i . '_time_start' ) ) ) );
						// echo 'Class @ '.$start.'<br />';

						// If class is later than current time
						if ( $start > $today->format('Gi') ) {

							// Get class info
							$class_id = esc_html( get_option( $option.'_' . $i . '_class' ) );
							$teacher_id = esc_html( get_option( $option.'_' . $i . '_teacher' ) );
							$start = date( $format, strtotime( get_option( $option.'_' . $i . '_time_start' ) ) );

							// Add class to array
							$classes[] = array(
								'class' => get_the_title( $class_id ),
								'teacher' => get_the_title( $teacher_id ),
								'time' => $start,
							);
						}
					}
					
				}
			}

			// If any classes were found for later that day, stop
			if ( !empty($classes) ) {
				break;
			}
		}

		// Add a day
		$today->modify('+1 day');
		$today->setTime(00, 00);

		// Increase option key
		$option_key++;
	}

	// echo $today->format('Y-m-d H:i:s').'<br />';

	// If any classes were found
	if ( !empty($classes) ) { ?>

		<aside class="upcoming-classes">

			<div class="upcoming-classes-content">

				<h4 class="widget-title"><?php _e('Upcoming class', 'satya'); ?></h4>
			<?php
			if ( isset($moon_phase) ) { ?>
				<div class="notice-moon-<?php echo $moon_phase; ?>">
					<p><?php
					printf( 
						__( 'No classes today due to the %2$s. <a href="/%1$s">Learn why.</a>', 'satya' ), get_permalink( get_page_by_title( 'Días de luna' ) ), $moon_phase_text );
					?></p>
				</div>
			<?php } else { ?>

				

				<div class="date">
					<span class="day-week">
						<?php echo date_i18n( 'l', $today->getTimestamp() ); ?>
					</span>
					<span class="day"><?php echo date_i18n( 'j', $today->getTimestamp() ); ?></span>
					<span class="month"><?php echo date_i18n( 'M', $today->getTimestamp() ); ?></span>
				</div>

				<div class="upcoming-class">
					<h5 class="class-name"><?php echo $classes[0]['class']; ?></h5>
					<span class="class-time"><?php echo $classes[0]['time']; ?></span><span class="class-teacher"><?php echo $classes[0]['teacher']; ?></span>
				</div>
			<?php } ?>
				<a href="<?php echo get_permalink( get_page_by_path( 'horarios' ) ); ?>" class="more-classes"><?php _e('See all classes', 'satya'); ?></a>
			</div>
		</aside>

	<?php }
}


?>