<?php
/**
 * Template part for displaying teh hero image and heading
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

?>

<header class="blog-header">

	<div class="article-header">

		<?php echo satya_event_date_column(); ?>

		<div>

			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

			<div class="entry-meta">
				<?php
				satya_posted_by();
				satya_posted_in__header();
				satya_tags();
				?>
			</div><!-- .entry-meta -->

		</div>

	</div>

	<?php get_template_part( 'template-parts/aside/classes','next' ); ?>

</header>