<?php
/**
 * Template part for displaying teh hero image and heading
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

?>

<header class="article-header simple-header">

	<?php if ( is_home() && ! is_front_page() ) { ?>

		<h1 class="page-title"><?php single_post_title(); ?></h1>

	<?php } else { ?>

		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

	<?php } ?>

	<?php get_template_part( 'template-parts/aside/classes','next' ); ?>

</header>