<?php
/**
 * Template part for displaying teh hero image and heading
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

?>

<header class="article-header hero-header">
	<!-- <div class="hero-container"> -->

		<?php
		if ( is_home() || is_front_page() ) { ?>

			<div class="header-branding">
				<a class="header-logo" href="<?php echo home_url(); ?>" rel="home" itemprop="url"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-satya-horizontal.svg" alt="Ashtanga Yoga Satya" /></a>
			</div>

			<?php if ( get_header_image() ) : ?>
			    <figure class="header-image">
			        <?php
			        /*
			        <img src="<?php header_image(); ?>" width="<?php echo absint( get_custom_header()->width ); ?>" height="<?php echo absint( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
			        */
			        // $img = get_header_image();
					// var_dump($img);
			        // echo satya_simple_srcset($img);
					$img = get_header_image_tag();
					echo $img;
			        ?>
			    </figure>
			<?php endif; ?>

			<div class="header-color-block"></div>
			<div class="header-overlay"></div>

			<div class="header-text">
				<?php
				$description = get_bloginfo( 'description', 'display' );
				if ( $description || is_customize_preview() ) :
					?>
						<h1 class="page-titlesite-description">
							<?php echo $description; ?>
						</h1>
				<?php endif; ?>
			</div><!-- .entry-header -->

		<?php 
		} else { 
		 
			if ( has_post_thumbnail( $post->ID ) ) {
				satya_post_thumbnail(); 
			} ?>

			<div class="header-color-block"></div>
			<div class="header-overlay"></div>
			<div class="header-text">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</div><!-- .entry-header -->

		<?php }

		get_template_part( 'template-parts/aside/classes','next' );

		?>
	<!-- </div> -->
</header>