<?php
/**
 * Block Name: Therapies
 *
 * This is the template that displays the therapies block.
 */

// get image field (array)
// $number_of_posts = intval(get_field('therapies_block_posts_per_page'));
$bg = get_field('feat_text_shape');
$text = get_field('feat_text');

$bg_class = 'bg-'.$bg;

// create id attribute for specific styling
// $id = 'therapies-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

// $all_posts = satya_get_posts($number_of_posts, 'therapy');
?>
<figure class="featured-text <?php echo $align_class.' '.$bg_class; ?>">
	<blockquote>
		<?php echo wpautop($text); ?>
	</blockquote>
</figure>