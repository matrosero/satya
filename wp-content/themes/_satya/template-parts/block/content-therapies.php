<?php
/**
 * Block Name: Therapies
 *
 * This is the template that displays the therapies block.
 */

// get image field (array)
$number_of_posts = intval(get_field('therapies_block_posts_per_page'));
$therapies_show_description = get_field('therapies_block_description');

// create id attribute for specific styling
$id = 'therapies-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

$all_posts = satya_get_posts($number_of_posts, 'therapy');
?>
<section class="therapies-grid <?php echo $align_class; ?>">
	<?php
	if ($all_posts->have_posts()) :
        while ($all_posts->have_posts()) : $all_posts->the_post();
            get_template_part( 'template-parts/content/therapy', 'loop' );
        endwhile;
        
    endif;
    wp_reset_postdata();
    ?>
</section>