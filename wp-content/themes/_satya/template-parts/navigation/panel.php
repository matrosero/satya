	<div id="cd-nav" class="offcanvas-panel">
		
		<a href="#" id="close-panel">
			<!-- <i class="icon-close"></i> --><span class="screen-reader-text">Close</span>
		</a>
		
		<div class="panel-container">
			<h2 class="screen-reader-text">Navigation</h2>

			<nav class="panel-navigation">
				<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'satya' ); ?></button> -->
				<?php
				wp_nav_menu( array(
					'theme_location' => 'offcanvas-nav',
					'menu_id'        => 'panel-menu',
					'container'		=> '',
				) );
				?>
			</nav><!-- #site-navigation -->

		
			<!-- <address>
				<ul class="cd-contact-info">
					<li><a href="mailto:info@myemail.co">info@myemail.co</a></li>
					<li>0244-12345678</li>
					<li>
						<span>MyStreetName 24</span>
						<span>W1234X</span>
						<span>London, UK</span>
					</li>
				</ul>
			</address> -->

		</div> <!-- .panel-container -->
	</div> <!-- .cd-nav -->