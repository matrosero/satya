<?php
/**
 * Template part for displaying intro for a single event
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */
?>

<section class="event-intro">

	<?php
	if ( has_post_thumbnail() ) { ?>
		<div class="event-image">
			<?php
			$sizes = satya_build_srcset_sizes('100vw',null,'33vw');

	        $srcset = satya_srcset( get_post_thumbnail_id( get_the_ID() ), 'large', $sizes, 'event-image' );

	        echo $srcset;
			?>
		</div>
	<?php } 

	if ( has_excerpt() ) { ?>
		<div class="event-summary">
			<?php echo the_excerpt(); ?>
		</div>
	<?php }

	?>

	<div class="event-details event-meta">
		
		<?php
		if( satya_is_future() ) {
			
			// Venue
			if ( get_post_meta( $post->ID, 'event_venue', true ) ) { ?>

				<div class="event-venue">
					<h6 class="meta-title"><?php _e( 'Venue', 'satya' ); ?></h6>

					<?php if ( get_post_meta( $post->ID, 'event_venue', true ) == 'satya' ) { ?>

						<p>Ashtanga Yoga Satya</p>

						<?php //echo apply_filters('the_content',get_option( 'options_contact_address' )); ?>
					<?php } else {
						echo '<p>'.esc_html(get_post_meta( $post->ID, 'event_venue', true )).'</p>';
					} ?>
				</div>

			<?php } 

			// Date
			if ( get_post_meta( $post->ID, 'event_date_type', true ) ) { 

				// Default display format
				$format = 'j \d\e F';

				// Get type of date
				$date_type = get_post_meta( $post->ID, 'event_date_type', true ); 

				?>

				<div class="event-date">
					
					<?php if ( $date_type == 'single-date' ) { ?>
						<h6 class="meta-title"><?php _e( 'Date', 'satya' ); ?></h6>
					<?php } else { ?>
						<h6 class="meta-title"><?php _e( 'Dates', 'satya' ); ?></h6>
					<?php } ?>


					<?php 

					// Single date
					if ( $date_type == 'single-date' ) { 

						$date = satya_event_single_date();

						echo satya_readable_event_dates($date);

						echo '</div>';

					
					// Several dates
					} elseif ( $date_type == 'several-dates' && get_post_meta( $post->ID, 'additional_dates', true ) ) { 

						$dates = satya_event_several_dates();

						echo '<ul>';

							foreach ($dates as $date) {
								
								echo '<li>'
									.satya_readable_event_dates($date)
									.'</li>';

							}

						echo '</ul>';
						echo '</div>';

					// Date range THIS ONE FIXED
					} elseif ( $date_type == 'date-range' && get_post_meta( $post->ID, 'event_date_end', 1 ) ) {

						$dates = satya_event_date_range();

						echo satya_readable_event_daterange($dates);

						// If weekdays chosen
						if ( get_post_meta( $post->ID, 'event_choose_weekdays', 1 ) && get_post_meta( $post->ID, 'event_weekdays', 1 ) ) {

							$weekdays = get_post_meta( $post->ID, 'event_weekdays', 1 );

							foreach ($weekdays as $key => $day) {
								if ( $day == 'Sunday' ) {
									$weekdays[$key] = strtolower( __('Sunday', 'satya') );
								} else {
									$weekdays[$key] = strtolower( __($day, 'satya') );
								}
							}

							$last  = array_slice($weekdays, -1);
							$first = join(', ', array_slice($weekdays, 0, -1));
							$both  = array_filter(array_merge(array($first), $last), 'strlen');

							echo '<p>'.ucfirst( join( __(' and ', 'satya'), $both) ).'</p>';
						}

						// Time
						echo '<p>'.satya_event_readable_time($dates).'</p>';

						echo '</div>';

					} ?>
					

			<?php } 

			// Pricing
			if ( get_post_meta( $post->ID, 'event_pricing', 1 ) ) { ?>
				<div class="event-pricing">
					<h6 class="meta-title screen-reader-text"><?php _e( 'Cost', 'satya' ); ?></h6>

					<ul class="event-prices">
						<?php
						$pricing = get_post_meta( $post->ID, 'event_pricing', 1 );
						for( $i = 0; $i < $pricing; $i++ ) {
							$currency = esc_html( get_post_meta( get_the_ID(), 'event_pricing_' . $i . '_currency', true ) );
							$price = (int) get_post_meta( get_the_ID(), 'event_pricing_' . $i . '_price', true );

							$price = number_format( $price , 0 , ',' , '.' );

							$description = esc_html( get_post_meta( get_the_ID(), 'event_pricing_' . $i . '_description', true ) );

							echo '<li><span class="price-amount">'.$currency.$price.'</span><span class="price-desc">'.$description.'</span></li>';

						}
					echo '</ul>';

				echo '</div>';
			}

		} else { ?>

			<div class="no-upcoming-dates">	
				<h6 class="meta-title"><?php _e('There\'s no upcoming dates scheduled for this event.', 'satya'); ?></h6>
				<?php 
				$form = do_shortcode( '[mc4wp_form id="827"]' );

				$replace = __('I want to be notified of future workshop and events.', 'satya');

				echo str_replace('Quiero recibir noticias de Ashtanga Yoga Satya', $replace, $form);
				?>
			</div>

		<?php } ?>

	</div>


	
</section><!-- .event-intro -->