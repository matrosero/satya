<?php
/**
 * Template part for displaying events in loop
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<a class="card-event" href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">

		<?php
        $sizes = satya_build_srcset_sizes('100vw','50vw','33vw');

        $srcset = satya_srcset( get_post_thumbnail_id( get_the_ID() ), 'large', $sizes, 'event-image' );

        echo $srcset;
        ?>

		<div class="event-info">

			<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>

			<?php
			if( satya_is_future() ) {
				echo satya_event_date_column(); 
			} 
			// the_field('event_date_end');
			// echo ' | '.get_field('event_date_type');
			?>

		</div>

	</a>

</article><!-- #post-<?php the_ID(); ?> -->
