<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">

		<?php
		if ( 'post' === get_post_type() ) {
			
			?>
			<div class="entry-info">
				<?php
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
				
				if ( 'post' === get_post_type() ) : ?>
					<div class="entry-meta">
						<?php
						
						// satya_posted_on();
						satya_posted_by();
						satya_posted_in__header();
						?>
					</div><!-- .entry-meta -->
				<?php endif; ?>
			</div><!-- .entry-info -->
			<?php 
			echo satya_event_date_column();
		} else {
			the_title( '<h2 class="entry-title">', '</h2>' );
		} ?>
	</header><!-- .entry-header -->


	<div class="entry-summary">
		<?php
		the_excerpt();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'satya' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php satya_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article> 
