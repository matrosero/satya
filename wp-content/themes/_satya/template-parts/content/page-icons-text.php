<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

if ( get_post_meta( $post->ID, 'icon_text_class_name', 1) ) {
	echo '<section class="icon-text '.get_post_meta( $post->ID, 'icon_text_class_name', 1).'">';
} else {
	echo '<section>';
} ?>

	<ul>
		<?php
		if ( get_post_meta( $post->ID, 'icon_text_group', 1) ) {

			$count = get_post_meta( $post->ID, 'icon_text_group', 1);

			for( $i = 0; $i < $count; $i++ ) {
				$icon = get_post_meta( get_the_ID(), 'icon_text_group_' . $i . '_icon', true );
				$text = get_post_meta( get_the_ID(), 'icon_text_group_' . $i . '_text', true );
				?>

				<li>
					<div class="icon"><?php
					echo wp_get_attachment_image( $icon, 'full' ); ?></div>
					<p><?php echo $text; ?></p>
				</li>

			<?php }
		} ?>
	</ul>
</section>


<?php if ( get_edit_post_link() ) : ?>
	<footer class="entry-footer">
		<?php
		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'satya' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
		?>
	</footer><!-- .entry-footer -->
<?php endif; ?>

