<?php
/**
 * Therapies Section
 *
 * This is the template that displays the therapies block.
 */

	/*
	 * The WordPress Query class.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/WP_Query
	 */
	$args = array(

		// Type & Status Parameters
		'post_type'   => 'therapy',
		'post_status' => 'publish',

		// Order & Orderby Parameters
		'order'               => 'DESC',
		'orderby'             => 'date',

		// Pagination Parameters
		'posts_per_page'         => -1,

		'cache_results'          => true,
		'update_post_term_cache' => true,
		'update_post_meta_cache' => true,

		'tax_query' => array(
			array(
				'taxonomy' => 'satya_therapist',
				'field'    => 'slug',
				'terms'    => 'satya',
			),
		),

	);

	$query = new WP_Query( $args );


?>
<section class="therapies-grid <?php echo $align_class; ?>">
	<?php
	if ($query->have_posts()) :
        while ($query->have_posts()) : $query->the_post();
            get_template_part( 'template-parts/content/therapy', 'loop' );
        endwhile;
        
    endif;
    wp_reset_postdata();
    ?>
</section>