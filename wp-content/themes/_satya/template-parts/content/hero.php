<?php
/**
 * Template part for displaying teh hero image and heading
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

?>

<?php 
if ( has_post_thumbnail( $post->ID ) ) { ?>

	<header class="article-header hero-header">
		<figure class="header-image post-thumbnail">
			<?php satya_post_thumbnail(); ?>
		</figure><!-- .post-thumbnail -->
		<div class="header-color-block"></div>
		<div class="header-text">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</div><!-- .entry-header -->
	</header>

<?php } ?>
