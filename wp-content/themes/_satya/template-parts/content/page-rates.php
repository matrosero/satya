<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

if ( !isset($post_id) ) {
	$post_id = get_the_ID();
}
?>

<section class="rates">
	<ul>
		<?php
		if ( get_post_meta( $post_id , 'rates', 1) ) {

			$count = get_post_meta( $post_id , 'rates', 1);

			for( $i = 0; $i < $count; $i++ ) {
				$title = get_post_meta( $post_id, 'rates_' . $i . '_title', true );
				$desc = get_post_meta( $post_id, 'rates_' . $i . '_desc', true );
				$rate = get_post_meta( $post_id, 'rates_' . $i . '_rate', true );
				?>

				<li>
					<h4 class="rate-title"><?php echo $title; ?>
						<span class="rate-amount">¢<?php echo $rate; ?></span>
					</h4>
					<p class="rate-description"><?php echo $desc; ?></p>
				</li>

			<?php }
		} ?>
	</ul>
</section>