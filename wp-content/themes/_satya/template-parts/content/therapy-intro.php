<?php
/**
 * Template part for displaying intro for a single event
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */
?>

<section class="therapy-intro">

	<?php
	if ( has_post_thumbnail() ) { ?>
		<div class="therapy-image">
			<?php the_post_thumbnail( 'full' ); ?>
		</div>

		<div class="therapy-summary">
			<?php
			if ( has_excerpt() ) {
				echo the_excerpt();
			} else {

			} ?>
		</div>
	<?php } ?>

	
</section><!-- .event-intro -->