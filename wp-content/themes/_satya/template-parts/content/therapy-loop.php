<?php
/**
 * Template part for displaying therapies in loop
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

$class = '';

if ( is_page_template( 'page-templates/therapies.php' ) && get_post_meta( $post->ID, 'grid_x2', true )) {
	$class = 'grid-2x';
}

?>

<article id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>
	<a class="banner" href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
		<?php 
		if ( is_singular('person') ) {
			$sizes = satya_build_srcset_sizes('100vw','50vw');
		} else {
			$sizes = satya_build_srcset_sizes('100vw',null,'50vw', '33vw');
		}
		
        $srcset = satya_srcset( get_post_thumbnail_id( get_the_ID() ), 'large', $sizes, 'banner-image' );

        echo $srcset;
		?>

		<div class="banner-text">

			<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>

			<?php
			if ( ! is_singular('person') ) { ?>
				
				<p class="entry-meta">
					<?php echo satya_list_associated_people( get_post_meta( $post->ID, 'therapist', true ) );?>		
				</p>

			<?php } ?>

		</div>

	</a>

</article><!-- #post-<?php the_ID(); ?> -->
