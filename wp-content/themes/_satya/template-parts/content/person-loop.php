<?php
/**
 * Template part for displaying people in loop
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

$class = '';

if ( is_page_template( 'page-templates/who.php' ) && !has_term( 'fundador', 'satya_role' ) ) {
	$class = 'support-staff';
}

?>

<li id="post-<?php the_ID(); ?>" <?php post_class($class); ?>>
	<a class="card-bio" href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">

		<?php 

		// Main team page
		if ( is_page_template( 'page-templates/who.php' ) ) {

			// Is a founder
			if ( has_term( 'fundador', 'satya_role' ) ) {
				$sizes = satya_build_srcset_sizes('350px','50vw','350px');

			// Everyone else
			} else {
				$sizes = satya_build_srcset_sizes('350px','33vw','235px');
			}

		// A bio/profile page
		} elseif ( is_singular('person') ) {
			$sizes = satya_build_srcset_sizes('100vw','50vw',null,'576px');

		// An event page
		// This in case there is only one teacher, at which point they'll be the biggest
		} elseif ( is_singular('event') ) {
			$sizes = satya_build_srcset_sizes('100vw','420px',null,'470px');

		// Everywhere else have it be 100vw
		} else {
			$sizes = satya_build_srcset_sizes('100vw');
		}

		$srcset = satya_srcset( get_post_thumbnail_id( get_the_ID() ), 'large', $sizes, 'bio-image' );

        echo $srcset;

		// the_post_thumbnail( 'full', array( 'class' => 'bio-image' ) );
		the_title( '<span class="bio-name">', '</span>' ); 
		?>

	</a>

</li><!-- #post-<?php the_ID(); ?> -->
