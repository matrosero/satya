<?php
/**
 * Template part for displaying an event's date and schedule
 *
 * @package satya
 */
?>

<?php

/*
 * if using polylang: 
 * https://stackoverflow.com/questions/50718114/wp-change-get-the-timed-f-y-date-format-with-polylang
 */

// printf( esc_html__( '<p>%1$s %2$s to %3$s</p>', 'satya' ), $start_date->format('n'), $start_date->format('j'), $end_date->format('j') );

if ( get_post_meta( $post->ID, 'event_date_type', true ) ) {
	
	// Default display format
	$format = 'j \d\e F';

	// Get type of date
	$date_type = get_post_meta( $post->ID, 'event_date_type', true );

	
	// Single date
	if ( $date_type == 'single-date' ) { ?>

		<h6><?php _e( 'Date', 'satya' ); ?></h6>

		<?php 
		$start_date = satya_capitalize_spanish_date( get_the_date( $format ) );


	// Several dates
	} elseif ( $date_type == 'several-dates' && get_post_meta( $post->ID, 'additional_dates', true ) ) { ?>

		<h6><?php _e( 'Dates', 'satya' ); ?></h6>

		<?php 
		$count = get_post_meta( $post->ID, 'additional_dates', true );

		for( $i = 0; $i < $count; $i++ ) {
			
			$date = get_post_meta( get_the_ID(), 'additional_dates_' . $i . '_date', true );
			$date = satya_capitalize_spanish_date( date_i18n( $format, strtotime( $date ) ) );

			$time_start = get_post_meta( get_the_ID(), 'additional_dates_' . $i . '_time_start', true );
		}

		var_dump(get_post_meta( $post->ID, 'additional_dates', true ));

	
	// Date range
	} elseif ( $date_type == 'date-range' ) { ?>

		<h6><?php _e( 'Dates', 'satya' ); ?></h6>

		<?php 

		// Get datetime objects
		$start_date = new DateTime( get_post_meta( $post->ID, 'event_date', true ) );
		$end_date = new DateTime( get_post_meta( $post->ID, 'event_date_end', true ) );

		// If dates are in the same month
		if ( $start_date->format('n') == $end_date->format('n') ) {

			echo '<p>Del ' . $start_date->format('j') . ' al ' . $end_date->format('j') .' de '. strtolower ( date_i18n ( 'F', $start_date->getTimestamp() ) ) .'</p>';

		// If dates are on different months
		} else {

			echo '<p>Del ' . strtolower ( date_i18n ( $format, $start_date->getTimestamp() ) ) . ' al ' . strtolower ( date_i18n ( $format, $end_date->getTimestamp() ) ) .'</p>';

		}

	}
} 


		// echo '<pre>';
		// var_dump(get_post_meta( $post->ID, 'event_choose_weekdays', true ));
		// var_dump(get_post_meta( $post->ID, 'event_weekdays', true ));
		// echo '</pre>';