<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

?>

<?php
if ( has_excerpt() ) { ?>

	<div class="bio-summary">
		<?php the_excerpt(); ?>
	</div>

<?php } ?>



<div class="entry-content">
	<?php

	the_content();

	// wp_link_pages( array(
	// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'satya' ),
	// 	'after'  => '</div>',
	// ) );
	?>
</div><!-- .entry-content -->


<?php if ( get_edit_post_link() ) : ?>
	<footer class="entry-footer">
		<?php satya_entry_footer(); ?>
	</footer><!-- .entry-footer -->
<?php endif; ?>
