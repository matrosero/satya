<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

?>

<div class="container-contact">
	<section class="contact-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'satya' ),
			'after'  => '</div>',
		) );
		?>
	</section><!-- .contact-content -->


	<?php
	if ( get_option( 'options_contact_phone' ) || get_option( 'options_contact_email' ) ) { ?>

		<section class="contact-details">
			<h3 class="section-title"><?php _e('Get in touch!', 'satya'); ?></h3>
			<address>
				
				<?php
				if ( get_option( 'options_contact_email' ) ) { 

					$email = esc_html( get_option( 'options_contact_email' ) ); ?>
					
					<span class="email"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></span>
				
				<?php }
				if ( satya_get_phone() ) { 
					echo satya_get_phone();
				} ?>

			</address>
		</section>

	<?php } ?>

	<?php
	if ( get_option( 'options_contact_address' ) ) { ?>
		<section class="contact-address">
			<h3 class="section-title"><?php _e('Where to find us', 'satya'); ?></h3>

			<?php echo apply_filters('the_content',get_option( 'options_contact_address' )); ?>
			
		</section>
	<?php } ?>
</div>

<section class="map">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3929.938978995931!2d-84.06896818536856!3d9.939035392892972!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8fa0e37d731f7fa1%3A0x40bc9623c44ed818!2sAshtanga+Yoga+SATYA!5e0!3m2!1sen!2scr!4v1551046312475" width="3000" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>
<?php
/*
$location = get_option( 'options_contact_map');

if( !empty($location) ):
?>
<div class="acf-map">
	<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
</div>

<?php endif; 
*/
?>




<?php if ( get_edit_post_link() ) : ?>
	<footer class="entry-footer">
		<?php
		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'satya' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
		?>
	</footer><!-- .entry-footer -->
<?php endif; ?>

