<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

?>

<div class ="team">
	<section class="founders">

		<?php
		if ( get_post_meta( $post->ID, 'who_main_heading', 1 ) ) { ?>
			<h2 class="section-title"><?php echo esc_html( get_post_meta( $post->ID, 'who_main_heading', 1 ) ); ?></h2>
		<?php } ?>

		<?php
		if ( get_post_meta( $post->ID, 'who_main_text', 1 ) ) { ?>
			<?php echo wpautop( get_post_meta( $post->ID, 'who_main_text', 1 ) ); ?>
		<?php } ?>

		<?php

			/*
			 * The WordPress Query class.
			 *
			 * @link http://codex.wordpress.org/Function_Reference/WP_Query
			 */
			$args = array(
		
				// Type & Status Parameters
				'post_type'   => 'person',
				'post_status' => 'publish',
		
				// Order & Orderby Parameters
				'order'               => 'DESC',
				'orderby'             => 'title',
		
				// Pagination Parameters
				'posts_per_page'         => -1,

				'meta_query' => array(
					array(
						'key' => '_thumbnail_id'
					)
				),
		
				// Taxonomy Parameters
				'tax_query' => array(
					// 'relation' => 'AND',
					array(
						'taxonomy'         => 'satya_role',
						'field'            => 'slug',
						'terms'            => array( 'fundador', 'profesor', 'terapeuta' ),
					),
				),
			);
		
		$query = new WP_Query( $args );

		if ($query->have_posts()) :

			echo '<ul class="people">';

			$do_not_duplicate = array();

	        while ($query->have_posts()) : $query->the_post();

	        	if ( has_term( 'fundador', 'satya_role' ) ) {
		            $do_not_duplicate[] = $post->ID;
		            get_template_part( 'template-parts/content/person', 'loop' );
		        }

	        endwhile;

	        $query->rewind_posts();

	        wp_reset_postdata();

	        echo '</ul>';
	        
	    endif;
		
		?>

	</section>

	<section class="support-staff">

		<?php
		if ( get_post_meta( $post->ID, 'who_others_heading', 1 ) ) { ?>
			<h2 class="section-title"><?php echo esc_html( get_post_meta( $post->ID, 'who_others_heading', 1 ) ); ?></h2>
		<?php } ?>

		<?php
		if ($query->have_posts()) :

			echo '<ul class="people">';

	        while ($query->have_posts()) : $query->the_post();

	        	if ( in_array( $post->ID, $do_not_duplicate ) ) continue;

		        get_template_part( 'template-parts/content/person', 'loop' );

	        endwhile;

	        wp_reset_postdata();

	        echo '</ul>';
	        
	    endif;
		
		?>

	</section>

</div>


<?php if ( get_edit_post_link() ) : ?>
	<footer class="entry-footer">
		<?php
		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'satya' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link">',
			'</span>'
		);
		?>
	</footer><!-- .entry-footer -->
<?php endif; ?>

