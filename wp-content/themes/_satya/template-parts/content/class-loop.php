<?php
/**
 * Template part for displaying classes in loop
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */
?>

<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<!-- <a class="card-event" href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"> -->


			<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>

			<?php 
			if ( has_excerpt() ) {
				the_excerpt();
			} else {
				the_content();
			}
			?>

	<!-- </a> -->

</li><!-- #post-<?php the_ID(); ?> -->
