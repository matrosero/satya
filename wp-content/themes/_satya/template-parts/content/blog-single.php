<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

?>

<div class="entry-content">
	<?php

	if ( has_post_thumbnail() ) {
		echo '<div class="featured-image">';

		$sizes = satya_build_srcset_sizes('100vw',null,'960px');

        $srcset = satya_srcset( get_post_thumbnail_id( get_the_ID() ), 'large', $sizes, 'event-image' );

        echo $srcset;

		echo '</div>';
	}

	the_content();

	wp_link_pages( array(
		'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'satya' ),
		'after'  => '</div>',
	) );
	?>
</div><!-- .entry-content -->

<?php 
if ( get_edit_post_link() ) : ?>
	<footer class="entry-footer">
		<?php satya_entry_footer(); ?>
	</footer><!-- .entry-footer -->
<?php endif; ?>