<?php
/*
 * Template Name: Icons & Text
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

get_header();

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php

	if ( !is_home() && !is_front_page() && !has_post_thumbnail( $post->ID ) ) {
		get_template_part( 'template-parts/header/simple' );
	} else {
		get_template_part( 'template-parts/header/hero' );
	} 
	
	while ( have_posts() ) :
		the_post();

		get_template_part( 'template-parts/content/page' );

		get_template_part( 'template-parts/content/page', 'icons-text' );

	endwhile; // End of the loop.
	?>

</article><!-- #post-<?php the_ID(); ?> -->

<?php
//get_sidebar();
get_footer();
