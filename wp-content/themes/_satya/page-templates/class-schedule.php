<?php
/*
 * Template Name: Class Schedule
 * 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package satya
 */

get_header();

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php

	get_template_part( 'template-parts/header/simple' );

	// echo do_shortcode( '[moon-next]' );
	
	while ( have_posts() ) :
		the_post();

		get_template_part( 'template-parts/content/page' );

	endwhile; // End of the loop.

	echo do_shortcode( '[class-schedule]' );

	if ( pll_current_language () == 'en' ) {
		$rates_page = get_page_by_title('Prices', OBJECT, 'page');
	} else {
		$rates_page = get_page_by_title('Tarifas', OBJECT, 'page');
	}

	$post_id = $rates_page->ID;

	include(locate_template('template-parts/content/page-rates.php'));
	
	?>

</article><!-- #post-<?php the_ID(); ?> -->

<?php
//get_sidebar();
get_footer();
